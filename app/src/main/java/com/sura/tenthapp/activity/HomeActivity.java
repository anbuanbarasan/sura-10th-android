package com.sura.tenthapp.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.inmobi.ads.InMobiBanner;
import com.sura.tenthapp.R;
import com.sura.tenthapp.fragment.FavouriteFragment;
import com.sura.tenthapp.fragment.HomeFragment;
import com.sura.tenthapp.fragment.FilesFragment;
import com.sura.tenthapp.fragment.SettingsFragment;
import com.sura.tenthapp.fragment.StoreLocatorFragment;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.SharedHelperModel;


import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends BaseActivity {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private RelativeLayout homeButton, filesButton, favButton, settingsButton, adLayout,storesButton;
    private ImageView homeIcon, filesIcon, favIcon, settingsIcon,storeIcon;
    private Dialog tutorial;
    private Intent i;
//    private AdView mAdView;
    private int selectedtab = 0;
    boolean doubleBackToExitPressedOnce = false;
    private FirebaseAnalytics mFirebaseAnalytics;
    public static LinearLayout bottomLayout;

    private InMobiBanner mBannerAd;
    public static final int BANNER_WIDTH = 320;
    public static final int BANNER_HEIGHT = 50;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);


//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.HOME_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();

//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });



        //setupBannerAd();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);


        tutorial = new Dialog(this);
        tutorial.requestWindowFeature(Window.FEATURE_NO_TITLE);
        tutorial.setContentView(R.layout.show_case_dialog);

        initViews();
        setHomeIconColor();
        bottomClickListeners(savedInstanceState);

        if (i.getStringExtra("from").equalsIgnoreCase("maintenance")) {
            bottomLayout.setVisibility(View.GONE);
            selectedtab = 0;
            Fragment fragmentnewview = new FavouriteFragment();
            FragmentManager frMan = getSupportFragmentManager();
            FragmentTransaction frTr = frMan.beginTransaction();
            frTr.replace(R.id.content, fragmentnewview);
            frTr.commit();
        } else {
            if (i.getStringExtra("pdf").equals("1")) {
                bottomLayout.setVisibility(View.VISIBLE);
                selectedtab = 1;
                Fragment fragmentnewview = new FilesFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();
                setFilesIconColor();
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
                selectedtab = 0;
                setHomeIconColor();
                Fragment fragmentnewview = new HomeFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();
            }
        }

//        if (new SharedHelperModel(this).getShowCaseStatus().equals("")) {
//            homeTutorialDialog();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: "+"destroyed" );
        new SharedHelperModel(this).setBooleanCheckHome("0");
        new SharedHelperModel(this).setBooleanCheckFiles("0");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: "+"pause" );
    }

    @Override
    public void onBackPressed() {
        if (selectedtab == 0) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        } else {
            selectedtab = 0;
            setHomeIconColor();
            Fragment fragmentnewview = new HomeFragment();
            FragmentManager frMan = getSupportFragmentManager();
            FragmentTransaction frTr = frMan.beginTransaction();
            frTr.replace(R.id.content, fragmentnewview);
            frTr.commit();
        }

    }

    private void showExitDialog(Context context) {
        final Dialog exitDialog = new Dialog(context);
        exitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exitDialog.setContentView(R.layout.exit_app_dialog);
        exitDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        exitDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        exitDialog.setCanceledOnTouchOutside(false);
        exitDialog.getWindow().setGravity(Gravity.CENTER);
        Button exitButton=exitDialog.findViewById(R.id.exitButton);
        Button cancelButton=exitDialog.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.dismiss();
            }
        });
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.dismiss();
                HomeActivity.super.onBackPressed();
            }
        });
        exitDialog.show();
    }

    private void homeTutorialDialog() {

        tutorial.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        tutorial.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tutorial.getWindow().setGravity(Gravity.CENTER);
        tutorial.setCancelable(false);
        final ImageView showCaseHomeIcon = tutorial.findViewById(R.id.showCaseHomeIcon);
        final ImageView showCaseFileIcon = tutorial.findViewById(R.id.showCaseFileIcon);
        final ImageView showCaseFavIcon = tutorial.findViewById(R.id.showCaseFavIcon);
        final ImageView showCaseKeyIcon = tutorial.findViewById(R.id.showCaseKeyIcon);
        RelativeLayout DialogLayout = tutorial.findViewById(R.id.DialogLayout);
        final RelativeLayout keyPathLayout = tutorial.findViewById(R.id.keyPathLayout);
        final RelativeLayout homePathLayout = tutorial.findViewById(R.id.homePathLayout);
        final RelativeLayout filesPathLayout = tutorial.findViewById(R.id.filesPathLayout);
        final RelativeLayout favPathLayout = tutorial.findViewById(R.id.favPathLayout);
        final TextView descriTutorialOne = tutorial.findViewById(R.id.descriTutorialOne);
        final TextView keyDescriTutorial = tutorial.findViewById(R.id.keyDescriTutorial);

        descriTutorialOne.setVisibility(View.VISIBLE);
        descriTutorialOne.setText(this.getResources().getString(R.string.findAllYourStudies));
        keyDescriTutorial.setVisibility(View.GONE);

        showCaseHomeIcon.setVisibility(View.VISIBLE);
        showCaseFileIcon.setVisibility(View.GONE);
        showCaseFavIcon.setVisibility(View.GONE);
        showCaseKeyIcon.setVisibility(View.GONE);

        homePathLayout.setVisibility(View.VISIBLE);
        filesPathLayout.setVisibility(View.GONE);
        favPathLayout.setVisibility(View.GONE);
        keyPathLayout.setVisibility(View.GONE);

        tutorial.show();
        DialogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showCaseHomeIcon.getVisibility() == View.VISIBLE) {
                    showCaseHomeIcon.setVisibility(View.GONE);
                    showCaseFileIcon.setVisibility(View.VISIBLE);
                    showCaseFavIcon.setVisibility(View.GONE);
                    showCaseKeyIcon.setVisibility(View.GONE);

                    homePathLayout.setVisibility(View.GONE);
                    filesPathLayout.setVisibility(View.VISIBLE);
                    favPathLayout.setVisibility(View.GONE);
                    keyPathLayout.setVisibility(View.GONE);

                    descriTutorialOne.setVisibility(View.VISIBLE);
                    descriTutorialOne.setText(getResources().getString(R.string.tutorial2));

                } else if (showCaseFileIcon.getVisibility() == View.VISIBLE) {
                    showCaseHomeIcon.setVisibility(View.GONE);
                    showCaseFileIcon.setVisibility(View.GONE);
                    showCaseFavIcon.setVisibility(View.VISIBLE);
                    showCaseKeyIcon.setVisibility(View.GONE);

                    homePathLayout.setVisibility(View.GONE);
                    filesPathLayout.setVisibility(View.GONE);
                    favPathLayout.setVisibility(View.VISIBLE);
                    keyPathLayout.setVisibility(View.GONE);

                    descriTutorialOne.setVisibility(View.VISIBLE);
                    descriTutorialOne.setText(getResources().getString(R.string.tutorial3));

                }

//                else if (showCaseFavIcon.getVisibility() == View.VISIBLE) {
//                    showCaseHomeIcon.setVisibility(View.GONE);
//                    showCaseFileIcon.setVisibility(View.GONE);
//                    showCaseFavIcon.setVisibility(View.GONE);
//                    showCaseKeyIcon.setVisibility(View.VISIBLE);
//
//                    homePathLayout.setVisibility(View.GONE);
//                    filesPathLayout.setVisibility(View.GONE);
//                    favPathLayout.setVisibility(View.GONE);
//                    keyPathLayout.setVisibility(View.VISIBLE);
//
//                    descriTutorialOne.setVisibility(View.GONE);
//                    keyDescriTutorial.setVisibility(View.VISIBLE);
//                    keyDescriTutorial.setText(getResources().getString(R.string.tutorial4));
//
//                }

                else if (showCaseFavIcon.getVisibility() == View.VISIBLE) {
                    showCaseHomeIcon.setVisibility(View.GONE);
                    showCaseFileIcon.setVisibility(View.GONE);
                    showCaseFavIcon.setVisibility(View.GONE);
                    showCaseKeyIcon.setVisibility(View.GONE);

                    homePathLayout.setVisibility(View.GONE);
                    filesPathLayout.setVisibility(View.GONE);
                    favPathLayout.setVisibility(View.GONE);
                    keyPathLayout.setVisibility(View.GONE);

                    keyDescriTutorial.setVisibility(View.GONE);
                    new SharedHelperModel(HomeActivity.this).setShowCaseStatus("1");
                    tutorial.dismiss();
                }
            }
        });
    }

    private void bottomClickListeners(final Bundle savedInstanceState) {
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                adLayout.setVisibility(View.VISIBLE);
                selectedtab = 0;
                setHomeIconColor();
                Fragment fragmentnewview = new HomeFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();
            }
        });
        filesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                adLayout.setVisibility(View.VISIBLE);
                selectedtab = 1;
                setFilesIconColor();
                Fragment fragmentnewview = new FilesFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();

            }
        });
        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                adLayout.setVisibility(View.VISIBLE);
                selectedtab = 1;
                setFavIconColor();
                Fragment fragmentnewview = new FavouriteFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                adLayout.setVisibility(View.GONE);
                selectedtab = 1;
                setSettingsIconColor();
                Fragment fragmentnewview = new SettingsFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();
            }
        });

        storesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedtab = 1;
                setStoreLocatorIconColor();
                Fragment fragmentnewview = new StoreLocatorFragment();
                FragmentManager frMan = getSupportFragmentManager();
                FragmentTransaction frTr = frMan.beginTransaction();
                frTr.replace(R.id.content, fragmentnewview);
                frTr.commit();
            }
        });

    }

    private void setSettingsIconColor() {
        homeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        filesIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        favIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        settingsIcon.setColorFilter(getResources().getColor(R.color.buttonColorBlue));
        storeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));

    }

    private void setFavIconColor() {

        homeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        filesIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        favIcon.setColorFilter(getResources().getColor(R.color.buttonColorBlue));
        settingsIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        storeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));

    }

    private void setFilesIconColor() {
        homeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        filesIcon.setColorFilter(getResources().getColor(R.color.buttonColorBlue));
        favIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        settingsIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        storeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));

    }

    private void setHomeIconColor() {
        homeIcon.setColorFilter(getResources().getColor(R.color.buttonColorBlue));
        filesIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        favIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        settingsIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        storeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));

    }

    private void setStoreLocatorIconColor() {
        homeIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        filesIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        favIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        settingsIcon.setColorFilter(getResources().getColor(R.color.bottomUnselectColor));
        storeIcon.setColorFilter(getResources().getColor(R.color.buttonColorBlue));

    }


    private void initViews() {
        homeButton = findViewById(R.id.homeButton);
        filesButton = findViewById(R.id.filesButton);
        favButton = findViewById(R.id.favButton);
        settingsButton = findViewById(R.id.settingsButton);
        bottomLayout = findViewById(R.id.bottomLayout);

        homeIcon = findViewById(R.id.homeIcon);
        filesIcon = findViewById(R.id.filesIcon);
        favIcon = findViewById(R.id.favIcon);
        settingsIcon = findViewById(R.id.settingsIcon);
        storeIcon = findViewById(R.id.storeIcon);
        storesButton = findViewById(R.id.storesButton);
        i = getIntent();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}