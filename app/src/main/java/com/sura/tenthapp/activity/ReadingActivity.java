package com.sura.tenthapp.activity;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.ads.AdView;
import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.reading.chapter_contents.BreadCrumbs;
import com.sura.tenthapp.utils.Database;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.MyWebChromeClient;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReadingActivity extends AppCompatActivity implements MyWebChromeClient.ProgressListener {

    private static final String TAG = "ReadingActivity";
    private RelativeLayout backButton, favButton, downloadButton;
    private Intent intent;
    private String tableName = "", keyName = "", id = "", chapterId = "", favCheck = "", chapterName = "", chapterNumber;
    private ImageView favIcon;
    private WebView webview;
    private TextView chapterNameText, chapterNumberText;
    //    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout;
    private ProgressDialog progrDialog;
    private LinearLayout titleBarLayout;
    private float mTouchPosition;
    private float mReleasePosition;
    public boolean isInActionmode = false;
    private String chapterContent;
    private static JSONArray jsonArray;
    private static JSONObject chapterContentJsonObject;
    private Database mHelper;
    private DownloadManager mgr = null;
    private long lastDownload = -1L;
    private static JSONArray imageLinksJsonArray;
    private static int count = 0;
    boolean isTitleBarHidden = false;
    ArrayList<String> lastDownloadList = new ArrayList<String>();
    int isLoop = 0;
    int isDownloaded = 0;
    int progressDownload = 0;
    TextView downloadButtonText;
    private ProgressBar mProgressBar;


    Dialog loaderOffline;
    private static String algorithm = "AES";
    String mykey = "surabooksurabook";
    SecretKey yourKey = new SecretKeySpec(mykey.getBytes(), "AES");

    int onBackCheck = 0;

    Dialog downloadDialog;
    Button dialogCancel;
    TextView downloadText;
    ProgressBar downloadView;
    ImageView successView;

    public String subjectName = "", subjectImageName = "", subjectDivisionImageName = "", subjectDivisionName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        mHelper = new Database(ReadingActivity.this);
        mgr = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        downloadDialog = new Dialog(ReadingActivity.this);
        downloadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        downloadDialog.setContentView(R.layout.download_dialog);
        downloadDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        downloadDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        downloadDialog.getWindow().setGravity(Gravity.CENTER);
        downloadDialog.setCancelable(false);
        downloadDialog.setCanceledOnTouchOutside(false);

        dialogCancel = downloadDialog.findViewById(R.id.cancelButton);
        downloadText = downloadDialog.findViewById(R.id.downloadText);
        downloadView = downloadDialog.findViewById(R.id.downloadView);
        successView = downloadDialog.findViewById(R.id.succesView);

        imageLinksJsonArray = new JSONArray();

//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                adLayout.setVisibility(View.VISIBLE);
//            }
//        });

        initViews();
        tableName = intent.getStringExtra("tableName");
        keyName = intent.getStringExtra("keyName");
        id = intent.getStringExtra("id");
        chapterId = intent.getStringExtra("chapterId");
        favCheck = intent.getStringExtra("favCheck");
        chapterName = intent.getStringExtra("chapterName");
        chapterNumber = intent.getStringExtra("chapterNumber");
        chapterContent = intent.getStringExtra("chapterContentJson");
        String isOffline = intent.getStringExtra("isOffline");
        Log.e(TAG, "isOffline: " + isOffline);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            chapterNameText.setText(Html.fromHtml(chapterName,Html.FROM_HTML_MODE_LEGACY));
        } else {
            chapterNameText.setText(Html.fromHtml(chapterName));
        }

//        chapterNumberText.setText(chapterNumber);




        setListeners();
        //setFavIcon();

        if (isOffline.equalsIgnoreCase("true")) {
            setOfflineWebview();
        } else {
            downloadButton.setVisibility(View.VISIBLE);
            subjectName = intent.getStringExtra("subjectName");
            subjectDivisionImageName = intent.getStringExtra("subjectDivisionImageName");
            subjectDivisionName = intent.getStringExtra("subjectDivisionName");
            subjectImageName = intent.getStringExtra("subjectImageName");
            if (!new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("true")) {
                downloadButton.setVisibility(View.VISIBLE);
            } else {
                downloadButton.setVisibility(View.GONE);
            }
            getResponse();
            setContinueStudy();
            Log.e(TAG, "onCreate: " + new SharedHelperModel(this).getIsOfferApplied());

        }
    }

    private void setContinueStudy() {
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);
        try {

            JSONArray jsonArray = null;

            Log.e(TAG, "onResume: " + new SharedHelperModel(this).getContinueStudyArray());


            if (new SharedHelperModel(this).getContinueStudyArray().equalsIgnoreCase("true")) {
                jsonArray = new JSONArray();

            } else {
                jsonArray = new JSONArray(sharedHelperModel.getContinueStudyArray());

            }

            Log.e(TAG, "setContinueStudy: " + jsonArray);

            if (jsonArray.length() != 0) {

                int continueCheck = 0;

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.optJSONObject(i).optString("chapter_id").equalsIgnoreCase(chapterId)) {
                        jsonArray.remove(i);
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("chapter_id", chapterId);
                        jsonObject.put("chapter_name", chapterName);
                        jsonObject.put("subject_division_name", subjectDivisionName);
                        jsonObject.put("subject_division_image_name", subjectDivisionImageName);
                        jsonObject.put("subject_name", subjectName);
                        jsonObject.put("subject_image_name", subjectImageName);
                        jsonObject.put("chapter_number", chapterNumber);
//                        jsonArray.put(0,jsonObject);
                        addToPos(0, jsonObject, jsonArray);
                        Log.e(TAG, "testing: " + subjectImageName + " " + subjectDivisionName + " " + subjectDivisionImageName);

                        Log.e(TAG, "sameChapterId: " + jsonArray);
                        continueCheck = 1;
                        break;
                    }
                }

                if (continueCheck == 0) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("chapter_id", chapterId);
                    jsonObject.put("chapter_name", chapterName);
                    jsonObject.put("subject_division_name", subjectDivisionName);
                    jsonObject.put("subject_division_image_name", subjectDivisionImageName);
                    jsonObject.put("subject_name", subjectName);
                    jsonObject.put("subject_image_name", subjectImageName);
                    jsonObject.put("chapter_number", chapterNumber);
//                        jsonArray.put(0,jsonObject);
                    addToPos(0, jsonObject, jsonArray);
                    continueCheck = 0;
                    Log.e(TAG, "testing: " + subjectImageName + " " + subjectDivisionName + " " + subjectDivisionImageName);

                    Log.e(TAG, "DifferentChapterId: " + jsonArray);
                } else {
                    continueCheck = 0;
                }

            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("chapter_id", chapterId);
                jsonObject.put("chapter_name", chapterName);
                jsonObject.put("subject_division_name", subjectDivisionName);
                jsonObject.put("subject_division_image_name", subjectDivisionImageName);
                jsonObject.put("subject_name", subjectName);
                jsonObject.put("subject_image_name", subjectImageName);
                jsonObject.put("chapter_number", chapterNumber);
                jsonArray.put(jsonObject);
                Log.e(TAG, "testing: " + subjectImageName + " " + subjectDivisionName + " " + subjectDivisionImageName);

                Log.e(TAG, "emptyArrayAdded: " + jsonArray);

            }
            sharedHelperModel.setContinueStudyArray(jsonArray.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addToPos(int pos, JSONObject jsonObj, JSONArray jsonArr) {

        try {
            for (int i = jsonArr.length(); i > pos; i--) {
                jsonArr.put(i, jsonArr.get(i - 1));
            }
            jsonArr.put(pos, jsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setOfflineWebview() {

        downloadButton.setVisibility(View.GONE);


        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= 19) {
            webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.setVerticalScrollBarEnabled(false);
        webview.setScrollBarSize(3);
        webview.setHorizontalScrollBarEnabled(false);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                Animation slide_down = AnimationUtils.loadAnimation(ReadingActivity.this,
                        R.anim.slide_down);

                Animation slide_up = AnimationUtils.loadAnimation(ReadingActivity.this,
                        R.anim.slide_up);

                final LinearLayout barLayout = findViewById(R.id.toolBarLayout);

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mTouchPosition = event.getY();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    isInActionmode = false;
//                    titleBarLayout.setVisibility(View.VISIBLE);
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

                    if (!isInActionmode) {
                        isInActionmode = true;
                        mReleasePosition = event.getY();

                        if (mTouchPosition - mReleasePosition > 0) {
                            // user scroll down
                            Log.d(TAG, "onTouch: Scrolleddown");

                            if (!isTitleBarHidden) {
                                isTitleBarHidden = true;

                                barLayout.startAnimation(slide_up);
//                                webview.startAnimation(slide_up);

                                slide_up.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        barLayout.setVisibility(View.GONE);
                                        titleBarLayout.setVisibility(View.GONE);
                                        findViewById(R.id.toolbar).setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });


                            } else {

                            }

                        } else {

                            if (isTitleBarHidden) {
                                isTitleBarHidden = false;

                                barLayout.setVisibility(View.VISIBLE);


                                titleBarLayout.setVisibility(View.VISIBLE);
                                findViewById(R.id.toolbar).setVisibility(View.VISIBLE);

                                barLayout.startAnimation(slide_down);
//                                webview.startAnimation(slide_down);
                                slide_down.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });


                            }
                            Log.d(TAG, "onTouch: ScrolledUP");

                            //user scroll up
                        }
                    }

                }
                return false;
            }
        });


        String baseDir = Environment.getExternalStorageDirectory().toString();
        String directory = baseDir + "/Android/data/com.sura.tenthapp/files/" + BreadCrumbs.subjectId + "/" + BreadCrumbs.chapterId + "/" + tableName + "/" + id + "/index.html";

        Log.e(TAG, "setOfflineWebview: " + directory);
        File file = new File(directory);

        final String decrypted = decodeFile(file);

        Log.e(TAG, "ReadingIdOffline: " + id);
//        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();


        webview.loadDataWithBaseURL("file:///storage/emulated/0/Android/data/com.sura.tenthapp/files/" + BreadCrumbs.subjectId + "/" + BreadCrumbs.chapterId + "/" + tableName + "/" + id + "/", decrypted, "text/html", "utf-8", null);

        webview.setWebChromeClient(new MyWebChromeClient(this));
        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadDataWithBaseURL("file:///storage/emulated/0/Android/data/com.sura.tenthapp/files/" + BreadCrumbs.subjectId + "/" + BreadCrumbs.chapterId + "/" + tableName + "/" + id + "/", decrypted, "text/html", "utf-8", null);

                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
            }

        });

//        webview.loadUrl("file:///storage/emulated/0/Android/data/com.sura.twelfthapp/files/" + BreadCrumbs.subjectId + "/" + BreadCrumbs.chapterId + "/" + id + "/index.html");
    }

    private static String getStorageDir() {
        jsonArray = BreadCrumbs.offlineJsonObject.optJSONArray("list_chapter_content");
        chapterContentJsonObject = BreadCrumbs.offlineJsonObject;
        String baseDir = Environment.getExternalStorageDirectory().toString();
        String directory = baseDir + "/Android/data/com.sura.tenthapp/files/" + chapterContentJsonObject.optString("subject_id") + "/" + chapterContentJsonObject.optString("chapter_id");
        return directory;
    }

    public static String getDestinationDirectory() {

        String defaultFileLocation = getStorageDir() + File.separator;

        return defaultFileLocation;
    }

    private void createNomediaFile() {


        String directoryLocation = getDestinationDirectory();

        File directory = new File(directoryLocation);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.e(TAG, "Could not create directory " + directoryLocation + ", mkdirs() returned false !");
            } else {
                Log.e(TAG, "createNomediaFile: " + "Success");

                File htmlFile = new File(directoryLocation + "chapterContent.json");

                try {
                    htmlFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(htmlFile);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
                    outputStreamWriter.append(chapterContentJsonObject.toString());
                    outputStreamWriter.close();
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "createHtmlFile: " + e.getMessage());
                }

//                mHelper.addToDatabase(chapterContentJsonObject.optString("subject_division_id"), chapterContentJsonObject.optString("chapter_id")
//                        , chapterContentJsonObject.optString("subject_name"), chapterContentJsonObject.optString("chapter_name"), "pending", directoryLocation + "chapterContent.json");

                downloadForOffline();
            }
        } else {

            File htmlFile = new File(directoryLocation + "chapterContent.json");

            try {
                htmlFile.createNewFile();
                FileOutputStream fOut = new FileOutputStream(htmlFile);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
                outputStreamWriter.append(chapterContentJsonObject.toString());
                outputStreamWriter.close();
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "createHtmlFile: " + e.getMessage());
            }

//            mHelper.addToDatabase(chapterContentJsonObject.optString("subject_division_id"), chapterContentJsonObject.optString("chapter_id")
//                    , chapterContentJsonObject.optString("subject_name"), chapterContentJsonObject.optString("chapter_name"), "pending", directoryLocation + "chapterContent.json");

            downloadForOffline();

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new SharedHelperModel(this).setReadedMode("1");
    }

    private void setFavIcon() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);

                loaderDialog.dismiss();

                if (response.optString("error_message").equals("1")) {
                    favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favourite_image));
                    favIcon.setTag(R.drawable.favourite_image);
                } else if (response.optString("error_message").equals("0")) {
                    favIcon.setImageDrawable(getResources().getDrawable(R.drawable.unfav_icon));
                    favIcon.setTag(R.drawable.unfav_icon);
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.e(TAG, "notifySuccessString: " + response);
            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("chapter_id", chapterId);
        Log.e(TAG, "getResponse: " + body);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postCheckFav, body, headers);
    }

    public void retryDialog(final String error) {
        final Dialog retryDialog = new Dialog(ReadingActivity.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton = retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();

                getResponse();

            }
        });
        retryDialog.show();
    }

    private void getResponse() {

//        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();

        Log.e(TAG, "ReadingIdOnline: " + id);

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
//                Log.e(TAG, "notifySuccessString: " + response);
                loaderDialog.dismiss();

                String bootstrapCssLink = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";
                String bootstrapJsLink = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js";
                String JqueryLink = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js";

                String replacedResponse = response.replaceAll(bootstrapCssLink, "file:///android_asset/bootstrap.min.css")
                        .replaceAll(bootstrapJsLink, "file:///android_asset/bootstrap.min.js")
                        .replaceAll(JqueryLink, "file:///android_asset/jquery.min.js");

                Log.e(TAG, "replacedResponse: " + replacedResponse);
                setWebView(response);
            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if (!error.equalsIgnoreCase("Parse Error.. Please Try Again..")) {
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("table_name", tableName);
        body.put("key_name", keyName);
        body.put("id", id);
        Log.e(TAG, "getResponse: " + body);
        mVolleyService.postDataStringVolley("POSTCALL", UrlHelper.getQuestion, body, headers);
    }

    private void setWebView(final String response) {


        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= 19) {
            webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.setVerticalScrollBarEnabled(false);
        webview.setScrollBarSize(3);
        webview.setHorizontalScrollBarEnabled(false);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webview.setWebChromeClient(new MyWebChromeClient(this));
        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadDataWithBaseURL(null, response, "text/html", "UTF-8", null);


                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);

            }


        });


        webview.loadDataWithBaseURL(null, response, "text/html", "UTF-8", null);

        webview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                Animation slide_down = AnimationUtils.loadAnimation(ReadingActivity.this,
                        R.anim.slide_down);

                Animation slide_up = AnimationUtils.loadAnimation(ReadingActivity.this,
                        R.anim.slide_up);

                final LinearLayout barLayout = findViewById(R.id.toolBarLayout);

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mTouchPosition = event.getY();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    isInActionmode = false;
//                    titleBarLayout.setVisibility(View.VISIBLE);
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

                    if (!isInActionmode) {
                        isInActionmode = true;
                        mReleasePosition = event.getY();

                        if (mTouchPosition - mReleasePosition > 0) {
                            // user scroll down
                            Log.d(TAG, "onTouch: Scrolleddown");

                            if (!isTitleBarHidden) {
                                isTitleBarHidden = true;

                                barLayout.startAnimation(slide_up);
//                                webview.startAnimation(slide_up);

                                slide_up.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        barLayout.setVisibility(View.GONE);
                                        titleBarLayout.setVisibility(View.GONE);
                                        findViewById(R.id.toolbar).setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });


                            } else {

                            }

                        } else {

                            if (isTitleBarHidden) {
                                isTitleBarHidden = false;

                                barLayout.setVisibility(View.VISIBLE);


                                titleBarLayout.setVisibility(View.VISIBLE);
                                findViewById(R.id.toolbar).setVisibility(View.VISIBLE);

                                barLayout.startAnimation(slide_down);
//                                webview.startAnimation(slide_down);
                                slide_down.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });


                            }
                            Log.d(TAG, "onTouch: ScrolledUP");

                            //user scroll up
                        }
                    }

                }
                return false;
            }
        });


    }


    private void setListeners() {

        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCancel.setVisibility(View.GONE);
                downloadText.setText("Initializing");
                downloadDialog.dismiss();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReadingActivity.super.onBackPressed();
            }
        });

        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (favCheck.equals("1")) {
                    unFavPostResponse();
                } else if (favCheck.equals("0")) {
                    favPostResponse();
                }
            }
        });

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (onBackCheck == 0) {
                    loaderOffline = LoaderDialog.showLoader(ReadingActivity.this);
                    count = 0;
                    isLoop = 0;
                    isDownloaded = 0;
                    if (checkWriteExternalPermission()) {
                        createNomediaFile();
                    } else {
                        Toast.makeText(ReadingActivity.this, "Please Enable Permission", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ReadingActivity.this, "Already Downloading..!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean checkWriteExternalPermission() {
        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void downloadForOffline() {

        JSONArray finalArray = new JSONArray();


        JSONArray chapterListArray = new JSONArray();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = new JSONObject();
            if (jsonArray.optJSONObject(i).optString("title_content").equalsIgnoreCase("1")) {
                try {
                    jsonObject.put("title", jsonArray.optJSONObject(i).optJSONArray("title"));
                    chapterListArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (jsonArray.optJSONObject(i).optString("title_content").equalsIgnoreCase("0")) {
                try {
                    jsonObject.put("key_name", jsonArray.optJSONObject(i).optString("key_name"));
                    jsonObject.put("table_name", jsonArray.optJSONObject(i).optString("tablename"));
                    jsonObject.put("id", jsonArray.optJSONObject(i).optString("id"));
                    finalArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.e(TAG, "chapterListArray: " + chapterListArray);

        JSONArray chapterListArray1 = new JSONArray();

        for (int i = 0; i < chapterListArray.length(); i++) {

            for (int i1 = 0; i1 < chapterListArray.optJSONObject(i).optJSONArray("title").length(); i1++) {
                JSONObject jsonObject = new JSONObject();
                if (chapterListArray.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("section").equalsIgnoreCase("1")) {
                    try {
                        jsonObject.put("title", chapterListArray.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optJSONArray("section_content"));
                        chapterListArray1.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (chapterListArray.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("section").equalsIgnoreCase("0")) {
                    try {
                        jsonObject.put("key_name", chapterListArray.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("key_name"));
                        jsonObject.put("table_name", chapterListArray.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("table_name"));
                        jsonObject.put("id", chapterListArray.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("id"));
                        finalArray.put(jsonObject);
                    } catch (JSONException e) {

                    }

                }
            }
        }

        Log.e(TAG, "chapterListArray1: " + chapterListArray1);

        JSONArray chapterListArray2 = new JSONArray();

        for (int i = 0; i < chapterListArray1.length(); i++) {
            if (chapterListArray1.optJSONObject(i).optJSONArray("title") != null)
                for (int i1 = 0; i1 < chapterListArray1.optJSONObject(i).optJSONArray("title").length(); i1++) {
                    JSONObject jsonObject = new JSONObject();
                    if (chapterListArray1.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("section").equalsIgnoreCase("1")) {

                        try {
                            jsonObject.put("title", chapterListArray1.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optJSONArray("section_content"));
                            chapterListArray2.put(jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        try {
                            jsonObject.put("key_name", chapterListArray1.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("key_name"));
                            jsonObject.put("table_name", chapterListArray1.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("table_name"));
                            jsonObject.put("id", chapterListArray1.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("id"));
                            finalArray.put(jsonObject);
                        } catch (JSONException e) {

                        }
                    }
                }
        }

        JSONArray chapterListArray3 = new JSONArray();

        for (int i = 0; i < chapterListArray2.length(); i++) {
            if (chapterListArray2.optJSONObject(i).optJSONArray("title") != null)
                for (int i1 = 0; i1 < chapterListArray2.optJSONObject(i).optJSONArray("title").length(); i1++) {
                    JSONObject jsonObject = new JSONObject();
                    if (chapterListArray2.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("section").equalsIgnoreCase("1")) {

                        try {
                            jsonObject.put("title", chapterListArray2.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optJSONArray("section_content"));
                            chapterListArray3.put(jsonObject);

                        } catch (JSONException e) {

                        }

                    } else {
                        try {
                            jsonObject.put("key_name", chapterListArray2.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("key_name"));
                            jsonObject.put("table_name", chapterListArray2.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("table_name"));
                            jsonObject.put("id", chapterListArray2.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("id"));
                            finalArray.put(jsonObject);
                        } catch (JSONException e) {

                        }

                    }

                }
        }

        JSONArray chapterListArray4 = new JSONArray();

        for (int i = 0; i < chapterListArray3.length(); i++) {
            if (chapterListArray3.optJSONObject(i).optJSONArray("title") != null)
                for (int i1 = 0; i1 < chapterListArray3.optJSONObject(i).optJSONArray("title").length(); i1++) {
                    JSONObject jsonObject = new JSONObject();
                    if (chapterListArray3.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("section").equalsIgnoreCase("1")) {

                        try {
                            jsonObject.put("title", chapterListArray3.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optJSONArray("section_content"));
                            chapterListArray4.put(jsonObject);

                        } catch (JSONException e) {

                        }

                    } else {
                        try {
                            jsonObject.put("key_name", chapterListArray3.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("key_name"));
                            jsonObject.put("table_name", chapterListArray3.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("table_name"));
                            jsonObject.put("id", chapterListArray3.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("id"));
                            finalArray.put(jsonObject);
                        } catch (JSONException e) {

                        }
                    }
                }
        }

        JSONArray chapterListArray5 = new JSONArray();

        for (int i = 0; i < chapterListArray4.length(); i++) {
            if (chapterListArray4.optJSONObject(i).optJSONArray("title") != null)
                for (int i1 = 0; i1 < chapterListArray4.optJSONObject(i).optJSONArray("title").length(); i1++) {
                    JSONObject jsonObject = new JSONObject();
                    if (chapterListArray4.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("section").equalsIgnoreCase("1")) {

                        try {
                            jsonObject.put("title", chapterListArray4.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optJSONArray("section_content"));
                            chapterListArray5.put(jsonObject);

                        } catch (JSONException e) {

                        }

                    } else {
                        try {
                            jsonObject.put("key_name", chapterListArray4.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("key_name"));
                            jsonObject.put("table_name", chapterListArray4.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("table_name"));
                            jsonObject.put("id", chapterListArray4.optJSONObject(i).optJSONArray("title").optJSONObject(i1).optString("id"));
                            finalArray.put(jsonObject);
                        } catch (JSONException e) {

                        }

                    }
                }
        }

        Log.e(TAG, "final: " + finalArray);

        makeDirectoryForEach(finalArray);

    }

    private void makeDirectoryForEach(JSONArray chapterListArray2) {

        for (int i = 0; i < chapterListArray2.length(); i++) {

            String directoryLocation = getStorageDir() + "/" + File.separator + chapterListArray2.optJSONObject(i).optString("table_name") + File.separator + chapterListArray2.optJSONObject(i).optString("id");

            File directory = new File(directoryLocation);
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                    Log.e(TAG, "Could not create directory " + directoryLocation + ", mkdirs() returned false !");
                } else {
                    Log.e(TAG, "createNomediaFile: " + "Success");

                    getResponseForOffline(chapterListArray2, i, chapterListArray2.length() - 1);
                }
            } else {
                getResponseForOffline(chapterListArray2, i, chapterListArray2.length() - 1);
            }
        }
    }

    private void getResponseForOffline(final JSONArray chapterListArray2, final int i, final int i1) {
        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.e(TAG, "getResponseForOffline: " + requestType);


                String[] splitList = requestType.split(",");

                String bootstrapCssLink = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";
                String bootstrapJsLink = "https://maxcdn.bootstrapcdn.co`m/bootstrap/4.0.0/js/bootstrap.min.js";
                String JqueryLink = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js";

                Document documents = Jsoup.parse(response);
                Elements linkss = documents.getElementsByTag("img");
                for (Element link : linkss) {
                    String linkHref = link.attr("src");

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("chapter_id", splitList[0]);
                        jsonObject.put("table_name", splitList[2]);
                        jsonObject.put("link", linkHref);
                        imageLinksJsonArray.put(jsonObject);
                    } catch (JSONException e) {

                    }

                    String fileName = linkHref.substring(linkHref.lastIndexOf('/') + 1);

                    response = response.replaceAll(linkHref, fileName);

                }


                String replacedResponse = response.replaceAll(bootstrapCssLink, "file:///android_asset/bootstrap.min.css")
                        .replaceAll(bootstrapJsLink, "file:///android_asset/bootstrap.min.js")
                        .replaceAll(JqueryLink, "file:///android_asset/jquery.min.js");


                String directoryLocation = getStorageDir() + File.separator + splitList[2] + File.separator + splitList[0] + File.separator;
                File htmlFile = new File(directoryLocation + "index.html");

                saveFile(replacedResponse, htmlFile);

//                try {
//                    htmlFile.createNewFile();
//                    FileOutputStream fOut = new FileOutputStream(htmlFile);
//                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
//                    outputStreamWriter.append(replacedResponse);
//                    outputStreamWriter.close();
//                    fOut.flush();
//                    fOut.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Log.e(TAG, "createHtmlFile: " + e.getMessage());
//                }


                Document document = Jsoup.parse(response);
                Elements links = document.getElementsByTag("img");
                for (Element link : links) {
                    String linkHref = link.attr("src");
//                    imageLinks.add(linkHref);
                    Log.e(TAG, "linkHref: " + linkHref);

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("chapter_id", splitList[0]);
                        jsonObject.put("table_name", splitList[2]);
                        jsonObject.put("link", linkHref);
                        imageLinksJsonArray.put(jsonObject);
                    } catch (JSONException e) {

                    }
                }

                count = count + 1;

                Log.e(TAG, "notifySuccessString: " + splitList[1] + " i value=" + count);

                if (splitList[1].equalsIgnoreCase(String.valueOf(count - 1))) {
                    Log.e(TAG, "imageLinksJsonArray: " + imageLinksJsonArray);
                    loaderOffline.dismiss();
//                    loaderOffline = LoaderDialog.showLoader(ReadingActivity.this);

                    downloadDialog.show();

                    downloadText.setText("Downloading..");

                    downloadView.setVisibility(View.VISIBLE);
                    successView.setVisibility(View.GONE);
                    onBackCheck = 1;
                    new doBackground().execute("");

                }


            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!" + requestType);
                loaderOffline.dismiss();
//                retryDialog(error);
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("table_name", chapterListArray2.optJSONObject(i).optString("table_name"));
        body.put("key_name", chapterListArray2.optJSONObject(i).optString("key_name"));
        body.put("id", chapterListArray2.optJSONObject(i).optString("id"));
        Log.e(TAG, "getResponse: " + body);
        mVolleyService.postDataStringVolley(chapterListArray2.optJSONObject(i).optString("id") + "," + i1 + "," + chapterListArray2.optJSONObject(i).optString("table_name"), UrlHelper.getQuestion, body, headers);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    private void downloadManager() {


        for (int i = 0; i < imageLinksJsonArray.length(); i++) {

            if (URLUtil.isValidUrl(imageLinksJsonArray.optJSONObject(i).optString("link"))) {

                String directory = "/Android/data/com.sura.tenthapp/files/" + chapterContentJsonObject.optString("subject_id")
                        + File.separator + chapterContentJsonObject.optString("chapter_id")
                        + File.separator + imageLinksJsonArray.optJSONObject(i).optString("table_name")
                        + File.separator + imageLinksJsonArray.optJSONObject(i).optString("chapter_id") + File.separator;

                Uri uri = Uri.parse(imageLinksJsonArray.optJSONObject(i).optString("link"));

                String filename = imageLinksJsonArray.optJSONObject(i).optString("link")
                        .substring(imageLinksJsonArray.optJSONObject(i).optString("link").lastIndexOf('/') + 1);

                lastDownload = mgr.enqueue(new DownloadManager.Request(uri)
                        .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                                DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(true)
                        .setTitle(filename)
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                        .setShowRunningNotification(true)
                        .setDescription("Downloading For Offline Reading..!")
                        .setDestinationInExternalPublicDir(directory,
                                filename));

                lastDownloadList.add(String.valueOf(lastDownload));

                Log.e(TAG, "lastDownloadInsideLoop: " + lastDownload);

            }
        }
    }

    public void queryStatus() {

        Log.e(TAG, "listSize: " + lastDownloadList.size());

        dialogCancel.setVisibility(View.VISIBLE);
        dialogCancel.setText("Hide");
        downloadButtonText.setText("Re-Download");


        if (isLoop < lastDownloadList.size()) {

            Cursor c = mgr.query(new DownloadManager.Query().setFilterById(Long.parseLong(lastDownloadList.get(isLoop))));

            if (c == null) {
                Toast.makeText(this, "Download not found!", Toast.LENGTH_LONG).show();
            } else {
                c.moveToFirst();
                Log.e(TAG, "queryStatus: " + lastDownloadList.get(isLoop) + " = " + statusMessage(c));

                if (statusMessage(c).equalsIgnoreCase("Download complete!")) {
                    downloadText.setText("Checking Downloading Files :" + isDownloaded + "/" + (lastDownloadList.size() - 1));
                    isDownloaded += 1;
                    progressDownload += 1;
                }
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isLoop += 1;
                    if (isDownloaded != lastDownloadList.size()) {
                        queryStatus();
                    }
                }
            }, 100);

        }


        Log.e(TAG, "isDownloaded: " + isDownloaded);
        Log.e(TAG, "lastDownloadList: " + lastDownloadList.size());

        if (isDownloaded == lastDownloadList.size()) {

            Log.e(TAG, "queryStatus: " + "Download Completed");

            String directoryLocation = getDestinationDirectory();

            int count = mHelper.checkDuplicate(chapterContentJsonObject.optString("subject_division_id"), chapterContentJsonObject.optString("chapter_id")
                    , directoryLocation + "chapterContent.json", "success");
            Log.e(TAG, "queryStatus: " + count);

            if (count == 0) {
                mHelper.addToDatabase(chapterContentJsonObject.optString("subject_division_id"), chapterContentJsonObject.optString("chapter_id")
                        , chapterContentJsonObject.optString("subject_name"), chapterContentJsonObject.optString("chapter_name"), "success", directoryLocation + "chapterContent.json");
            }
            mHelper.close();
            onBackCheck = 0;
            downloadView.setVisibility(View.GONE);
            successView.setVisibility(View.VISIBLE);

            downloadText.setText("Download Completed");
            dialogCancel.setText("ok");

            Toast.makeText(ReadingActivity.this, "Download Complete", Toast.LENGTH_SHORT).show();

        } else {

            if (isLoop == lastDownloadList.size()) {
                isLoop = 0;
                isDownloaded = 0;
                queryStatus();
            }
        }


    }

    private String statusMessage(Cursor c) {
        String msg = "???";

        switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Download failed!";
                break;

            case DownloadManager.STATUS_PAUSED:
                msg = "Download paused!";
                break;

            case DownloadManager.STATUS_PENDING:
                msg = "Download pending!";
                break;

            case DownloadManager.STATUS_RUNNING:
                msg = "Download in progress!";
                break;

            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Download complete!";
                break;

            default:
                msg = "Download is nowhere in sight";
                break;
        }

        return (msg);
    }


    private void unFavPostResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {
                    favCheck = "0";
                    favIcon.setImageDrawable(getResources().getDrawable(R.drawable.unfav_icon));
                    Toast.makeText(ReadingActivity.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ReadingActivity.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {

                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("chapter_id", chapterId);

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postRemoveFav, body, headers);
    }

    private void favPostResponse() {
        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {
                    favCheck = "1";
                    favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favourite_image));
                    Toast.makeText(ReadingActivity.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ReadingActivity.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("chapter_id", chapterId);
        body.put("standard", getResources().getString(R.string.standard));

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postAddFav, body, headers);
    }


    private void initViews() {
        backButton = findViewById(R.id.backButton);
        favButton = findViewById(R.id.favButton);
        favIcon = findViewById(R.id.favIcon);
        webview = findViewById(R.id.webview);
        chapterNameText = findViewById(R.id.chapterNameText);
        chapterNumberText = findViewById(R.id.chapterNumberText);
        titleBarLayout = findViewById(R.id.titleBarLayout);
        downloadButton = findViewById(R.id.downloadButton);
        downloadButtonText = findViewById(R.id.downloadButtonText);
        mProgressBar = findViewById(R.id.progressBar);
        intent = getIntent();
        progrDialog = new ProgressDialog(ReadingActivity.this);


    }


//    public static SecretKey generateKey() throws NoSuchAlgorithmException {
//        // Generate a 256-bit key
//        final int outputKeyLength = 256;
//        SecureRandom secureRandom = new SecureRandom();
//        // Do *not* seed secureRandom! Automatically seeded from system entropy.
//        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
//        keyGenerator.init(outputKeyLength, secureRandom);
//        yourKey = keyGenerator.generateKey();
//        return yourKey;
//    }

    public static byte[] encodeFile(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] encrypted = null;
        byte[] data = yourKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length,
                algorithm);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(
                new byte[cipher.getBlockSize()]));
        encrypted = cipher.doFinal(fileData);
        return encrypted;
    }

    public static byte[] decodeFile(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] decrypted = null;
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, yourKey, new IvParameterSpec(
                new byte[cipher.getBlockSize()]));
        decrypted = cipher.doFinal(fileData);
        return decrypted;
    }

    void saveFile(String stringToSave, File file) {
        try {
//            File file = new File(Environment.getExternalStorageDirectory()
//                    + File.separator, encryptedFileName);
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(file));
//            yourKey = generateKey();
            Log.e(TAG, "saveFile: " + yourKey.toString());
            byte[] filesBytes = encodeFile(yourKey, stringToSave.getBytes());
            bos.write(filesBytes);
            bos.flush();
            bos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String decodeFile(File path) {
        String str = "";
        try {
            byte[] decodedData = decodeFile(yourKey, readFile(path));
            str = new String(decodedData);
            System.out.println("DECODED FILE CONTENTS : " + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public byte[] readFile(File file) {
        byte[] contents = null;

//        File file = new File(Environment.getExternalStorageDirectory()
//                + File.separator, encryptedFileName);
        int size = (int) file.length();
        contents = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(file));
            try {
                buf.read(contents);
                buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return contents;
    }


    private class doBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            downloadManager();

            return "executed";
        }

        @Override
        protected void onPostExecute(String s) {
            queryStatus();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onUpdateProgress(int progressValue) {

        mProgressBar.setProgress(progressValue);
        if (progressValue == 100) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }

    }


}