package com.sura.tenthapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.sura.tenthapp.R;

public class CommingSoonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comming_soon);
    }
}
