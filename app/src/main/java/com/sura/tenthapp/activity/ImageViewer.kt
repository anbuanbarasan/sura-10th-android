package com.sura.tenthapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.sura.tenthapp.R
import kotlinx.android.synthetic.main.activity_image_viewer.*

class ImageViewer : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)
        Glide.with(this).load(intent.getStringExtra("link")).error(R.drawable.logo).into(image)

    }
}
