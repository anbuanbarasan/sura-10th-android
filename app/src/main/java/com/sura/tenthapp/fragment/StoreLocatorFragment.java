package com.sura.tenthapp.fragment;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.adapter.StoreLocatorAdapter;
import com.sura.tenthapp.utils.DistrictModel;
import com.sura.tenthapp.utils.DistrictSpinnerAdapter;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class StoreLocatorFragment extends Fragment {

    private AppCompatSpinner districtSpinner;
    private AppCompatEditText searchEditText;
    private RecyclerView storesRecycler;

    public StoreLocatorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_store_locator, container, false);
        initViews(view);
        getDistrictList();
        return view;
    }

    private void initViews(View view) {
        districtSpinner=view.findViewById(R.id.districtSpinner);
        searchEditText=view.findViewById(R.id.searchEditText);
        storesRecycler=view.findViewById(R.id.storesRecycler);
    }

    private void getDistrictList() {

        final Dialog loaderDialog = LoaderDialog.showLoader(getActivity());

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                loaderDialog.dismiss();

                if (response.optString("error").equals("false")) {
                    JSONArray jsonArray=response.optJSONArray("district_list");
                    ArrayList<DistrictModel> districtModelArrayList=new ArrayList<>();
                    DistrictModel allDistrictModel=new DistrictModel();
                    allDistrictModel.setId(0);
                    allDistrictModel.setDistrictName("All District");
                    districtModelArrayList.add(allDistrictModel);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        DistrictModel districtModel=new DistrictModel();
                        districtModel.setDistrictName(jsonArray.optJSONObject(i).optString("district_name"));
                        districtModel.setId(jsonArray.optJSONObject(i).optInt("district_id"));
                        districtModelArrayList.add(districtModel);
                    }

                    final DistrictSpinnerAdapter spinnerArrayAdapter = new DistrictSpinnerAdapter(getActivity().getBaseContext(),
                            android.R.layout.simple_spinner_item, districtModelArrayList);
                    districtSpinner.setAdapter(spinnerArrayAdapter);
                    districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view,
                                                   int position, long id) {
                            DistrictModel user = spinnerArrayAdapter.getItem(position);
                            getStoreList(user.getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });



                } else {
                    Toast.makeText(getActivity(), response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };


        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
//        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
//        body.put("device_id", sharedHelperModel.getDeviceToken());

        mVolleyService.getDataVolley("GETCALL", UrlHelper.districts_10, body);
    }

    private void getStoreList(Integer id) {
        final Dialog loaderDialog = LoaderDialog.showLoader(getActivity());

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                loaderDialog.dismiss();

                if (response.optString("error").equals("false")) {
                    setAdapter(response.optJSONArray("store_list"));
                } else {
                    Toast.makeText(getActivity(), response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };


        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
//        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("district_id", String.valueOf(id));

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.storelist_10, body,headers);
    }

    private void setAdapter(final JSONArray store_list) {
        storesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        final StoreLocatorAdapter storeLocatorAdapter=new StoreLocatorAdapter(getActivity(),store_list);
        storesRecycler.setAdapter(storeLocatorAdapter);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                JSONArray updated=new JSONArray();
                for (int i3 = 0; i3 < store_list.length(); i3++) {
                    if(store_list.optJSONObject(i3).optString("store_name").toLowerCase().contains(editable.toString().toLowerCase())){
                        updated.put(store_list.optJSONObject(i3));
                    }
                }
                storeLocatorAdapter.updateItems(updated);
            }
        });
    }

    public void retryDialog(final String error) {
        try {
            final Dialog retryDialog = new Dialog(getActivity());
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            Button retryButton = retryDialog.findViewById(R.id.retryButton);
            TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(error);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();

                    getDistrictList();

                }
            });
            retryDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}


