package com.sura.tenthapp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.HomeActivity;
import com.sura.tenthapp.activity.NotificationActivity;
import com.sura.tenthapp.modules.reading.continue_reading.ContinueReadingAdapter;
import com.sura.tenthapp.adapter.PagerAdapter;
import com.sura.tenthapp.modules.reading.subject_list_home.SubjectsHomeAdapter;
import com.sura.tenthapp.utils.AutoScrollViewPager;
import com.sura.tenthapp.utils.KeyDetailsDialog;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class HomeFragment extends Fragment {

    private static final String TAG = "Home";
    private RecyclerView continueStudyRecyclerView, subjectsRecyclerView;
    private RecyclerView.Adapter continueStudyAdapter, subjectsAdapter;
    private PagerAdapter pagerAdapter;
    private AutoScrollViewPager viewPager;

    private LinearLayout continueStudyLayout;
    private TextView keysText;
    private ScrollingPagerIndicator indicator;
    private String version;
    private NestedScrollView nestedScrollView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout offerButton;
    private LinearLayout keyDetails;
    private ImageView downArrow;
    private ImageView notification;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.home_fragment, container, false);

        initViews(view);
        keyDialogListener(view);

        swipeRefresh();
        scrollListener();
        if (new SharedHelperModel(getActivity()).getBooleanCheckHome().equalsIgnoreCase("1")) {
            String response = new SharedHelperModel(getActivity()).getHomeStudyContent();
            try {
                JSONObject jsonObject = new JSONObject(response);
                setSubjectsAdapter(jsonObject);
                /* if (jsonObject.optString("continue_study").toLowerCase().equals("no list")) {
                    continueStudyLayout.setVisibility(View.GONE);
                } else {
                    continueStudyLayout.setVisibility(View.VISIBLE);
                    setContinueStudyAdapter(jsonObject.optJSONArray("continue_study"));
                }*/

                JSONArray jsonArray = new JSONArray(new SharedHelperModel(getActivity()).getContinueStudyArray());

                if (jsonArray.length() == 0) {
                    continueStudyLayout.setVisibility(View.GONE);
                } else {
                    continueStudyLayout.setVisibility(View.VISIBLE);
                    setContinueStudyAdapter(jsonArray);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            String adsResponse=new SharedHelperModel(getActivity()).getHomeAds();
            try {
                JSONObject jsonObject=new JSONObject(adsResponse);

                if (isAdded()) {
                    pagerAdapter = new PagerAdapter(getChildFragmentManager());

                    try{
                        if(jsonObject.optJSONObject("announcement").optString("offer").equalsIgnoreCase("1")){
                            pagerAdapter.addFragment(AnnouncementFragment.newInstance(jsonObject.optJSONObject("announcement").optString("short_description"),jsonObject.optJSONObject("announcement").optString("long_description"),jsonObject.optJSONObject("announcement").optString("url"),jsonObject.optJSONObject("announcement").optString("type")));
                            new SharedHelperModel(getActivity()).setIsOfferApplied("true");
                            referral_concept_enabled= Boolean.parseBoolean(jsonObject.optString("is_referral_concept_enabled"));

                            if(referral_concept_enabled){
                                if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                                    keyDetails.setVisibility(View.GONE);
                                }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                                    keyDetails.setVisibility(View.VISIBLE);
                                }
                            }else{
                                keyDetails.setVisibility(View.GONE);
                            }
                        }else if(jsonObject.optJSONObject("announcement").optString("offer").equalsIgnoreCase("0")){
                            new SharedHelperModel(getActivity()).setIsOfferApplied("false");
                            referral_concept_enabled= Boolean.parseBoolean(jsonObject.optString("is_referral_concept_enabled"));

                            if(referral_concept_enabled){
                                if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                                    keyDetails.setVisibility(View.GONE);
                                }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                                    keyDetails.setVisibility(View.VISIBLE);
                                }
                            }else{
                                keyDetails.setVisibility(View.GONE);
                            }
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                    for (int i = 0; i < jsonObject.optJSONArray("list_ads").length(); i++) {
                        String title = jsonObject.optJSONArray("list_ads").optJSONObject(i).optString("title");
                        String descri = jsonObject.optJSONArray("list_ads").optJSONObject(i).optString("text");
                        String imageUrl = jsonObject.optJSONArray("list_ads").optJSONObject(i).optString("image");
                        String link = jsonObject.optJSONArray("list_ads").optJSONObject(i).optString("link");
                        pagerAdapter.addFragment(PagerFragment.newInstance(title, descri, imageUrl, link));
                    }

                    viewPager.startAutoScroll();
                    viewPager.setInterval(10000);
                    viewPager.setCycle(true);
                    viewPager.setStopScrollWhenTouch(true);
                    viewPager.setAdapter(pagerAdapter);
                    indicator.attachToPager(viewPager);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            try {
                Log.e(TAG, "HomeFragment: " + "working");
                getVersionCode();
                getVolleyResponse();
                getAdsVolleyResponse();
                //setRecyclerViewAnimation();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "onCreateView: " + e.getMessage());
            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    private void scrollListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {
                        //hide
                        downArrow.setVisibility(View.GONE);

                    }
                    if(scrollY < oldScrollY){
                        //show
                        downArrow.setVisibility(View.VISIBLE);

                    }


                }
            });
        }
    }


    private void swipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    Log.e(TAG, "HomeFragment: " + "working");
                    getVersionCode();
                    getVolleyResponse();
                    getAdsVolleyResponse();
                    //setRecyclerViewAnimation();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onCreateView: " + e.getMessage());
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    private void setRecyclerViewAnimation() {
//        subjectsRecyclerView.addOnScrollListener(new HideShowScrollListener() {
//            @Override
//            public void onHide() {
//                HomeActivity.bottomLayout.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onShow() {
//                HomeActivity.bottomLayout.setVisibility(View.VISIBLE);
//
//            }
//        });

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                Animation slide_down = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_down);

                Animation slide_up = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_up);


                if (scrollY > oldScrollY) {
                    Log.e(TAG, "Scroll DOWN");
//                    HomeActivity.bottomLayout.startAnimation(slide_down);
//                    HomeActivity.bottomLayout.animate().setInterpolator(new AccelerateDecelerateInterpolator()).scaleX(0).scaleY(0);
                    HomeActivity.bottomLayout.setVisibility(View.GONE);
//                    slideDown(HomeActivity.bottomLayout);

                }
                if (scrollY < oldScrollY) {
                    Log.e(TAG, "Scroll UP");
//                    HomeActivity.bottomLayout.startAnimation(slide_up);
//                    HomeActivity.bottomLayout.animate().setInterpolator(new AccelerateDecelerateInterpolator()).scaleX(1).scaleY(1);
                    HomeActivity.bottomLayout.setVisibility(View.VISIBLE);
//                    slideUp(HomeActivity.bottomLayout);

                }

                if (scrollY == 0) {
                    Log.e(TAG, "TOP SCROLL");
//                    HomeActivity.bottomLayout.startAnimation(slide_up);
                    HomeActivity.bottomLayout.setVisibility(View.VISIBLE);
                }

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    Log.e(TAG, "BOTTOM SCROLL");
//                    HomeActivity.bottomLayout.startAnimation(slide_up);
//                    HomeActivity.bottomLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void slideUp(final View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    private void getVersionCode() {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
            Log.e(TAG, "getVersionCode: " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getAdsVolleyResponse() {

//        LoaderDialog.showLoader(getActivity());
        final SharedHelperModel sharedHelperModel = new SharedHelperModel(getActivity());
        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
//                LoaderDialog.dismissLoader();

                try {

                    if (response.optString("error").equals("false")) {

                        sharedHelperModel.setHomeAds(response.toString());

                        if (isAdded()) {

                            pagerAdapter = new PagerAdapter(getChildFragmentManager());

                            try{
                                if(response.optJSONObject("announcement").optString("offer").equalsIgnoreCase("1")){
                                    pagerAdapter.addFragment(AnnouncementFragment.newInstance(response.optJSONObject("announcement").optString("short_description"),response.optJSONObject("announcement").optString("long_description"),response.optJSONObject("announcement").optString("url"),response.optJSONObject("announcement").optString("type")));
                                    new SharedHelperModel(getActivity()).setIsOfferApplied("true");
                                    referral_concept_enabled= Boolean.parseBoolean(response.optString("is_referral_concept_enabled"));

                                    if(referral_concept_enabled){
                                        if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                                            keyDetails.setVisibility(View.GONE);
                                        }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                                            keyDetails.setVisibility(View.VISIBLE);
                                        }
                                    }else{
                                        keyDetails.setVisibility(View.GONE);
                                    }
                                }else if(response.optJSONObject("announcement").optString("offer").equalsIgnoreCase("0")){
                                    new SharedHelperModel(getActivity()).setIsOfferApplied("false");
                                    referral_concept_enabled= Boolean.parseBoolean(response.optString("is_referral_concept_enabled"));

                                    if(referral_concept_enabled){
                                        if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                                            keyDetails.setVisibility(View.GONE);
                                        }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                                            keyDetails.setVisibility(View.VISIBLE);
                                        }
                                    }else{
                                        keyDetails.setVisibility(View.GONE);
                                    }
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                            }


                            for (int i = 0; i < response.optJSONArray("list_ads").length(); i++) {

                                String title = response.optJSONArray("list_ads").optJSONObject(i).optString("title");
                                String descri = response.optJSONArray("list_ads").optJSONObject(i).optString("text");
                                String imageUrl = response.optJSONArray("list_ads").optJSONObject(i).optString("image");
                                String link = response.optJSONArray("list_ads").optJSONObject(i).optString("link");
                                pagerAdapter.addFragment(PagerFragment.newInstance(title, descri, imageUrl, link));
                            }
                            viewPager.startAutoScroll();
                            viewPager.setInterval(10000);
                            viewPager.setCycle(true);
                            viewPager.setStopScrollWhenTouch(true);
                            viewPager.setAdapter(pagerAdapter);
                            indicator.attachToPager(viewPager);
                        }
                    } else {
                        Toast.makeText(getActivity(), response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "notifySuccess: " + e.getMessage());
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
//                LoaderDialog.dismissLoader();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.getDataVolley("GETCALL", UrlHelper.getAds, headers);

    }


    @Override
    public void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(getActivity()).getKeysCount() + " Keys");
        /*if (new SharedHelperModel(getActivity()).getReadedMode().equals("1")) {
            Log.e(TAG, "getReadingModeOnResume: " + new SharedHelperModel(getActivity()).getReadedMode());
            new SharedHelperModel(getActivity()).setReadedMode("0");
            getVolleyResponse();
        }*/
        try {
            Log.e(TAG, "onResume: "+new SharedHelperModel(getActivity()).getContinueStudyArray() );
            JSONArray jsonArray = new JSONArray(new SharedHelperModel(getActivity()).getContinueStudyArray());

            if (jsonArray.length() == 0) {
                continueStudyLayout.setVisibility(View.GONE);
            } else {
                continueStudyLayout.setVisibility(View.VISIBLE);
                setContinueStudyAdapter(jsonArray);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
//        LoaderDialog.dismissLoader();
    }

    public void retryDialog(final String text) {
        try {
            final Dialog retryDialog = new Dialog(getActivity());
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            Button retryButton = retryDialog.findViewById(R.id.retryButton);
            TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(text);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();

                    getVolleyResponse();

                }
            });
            retryDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(getActivity());
        final SharedHelperModel sharedHelperModel = new SharedHelperModel(getActivity());
        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();

                if (response.optString("error").equals("false")) {

                    sharedHelperModel.setBooleanCheckHome("1");
                    sharedHelperModel.setHomeStudyContent(response.toString());

                    if (response.optString("update").equalsIgnoreCase("0")) {
                        setSubjectsAdapter(response);
                         /*if (response.optString("continue_study").toLowerCase().equals("no list")) {
                            continueStudyLayout.setVisibility(View.GONE);
                        } else {
                            continueStudyLayout.setVisibility(View.VISIBLE);
                            setContinueStudyAdapter(response.optJSONArray("continue_study"));
                        }*/
                        try {
                            JSONArray jsonArray = new JSONArray(new SharedHelperModel(getActivity()).getContinueStudyArray());
                            if (jsonArray.length() == 0) {
                                continueStudyLayout.setVisibility(View.GONE);
                            } else {
                                continueStudyLayout.setVisibility(View.VISIBLE);
                                setContinueStudyAdapter(jsonArray);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (response.optString("update").equalsIgnoreCase("1")) {
                        showAppUpdateDialog();
                    } else {
                        setSubjectsAdapter(response);
                         /*if (response.optString("continue_study").toLowerCase().equals("no list")) {
                            continueStudyLayout.setVisibility(View.GONE);
                        } else {
                            continueStudyLayout.setVisibility(View.VISIBLE);
                            setContinueStudyAdapter(response.optJSONArray("continue_study"));
                        }*/
                        try {
                            JSONArray jsonArray = new JSONArray(new SharedHelperModel(getActivity()).getContinueStudyArray());
                            if (jsonArray.length() == 0) {
                                continueStudyLayout.setVisibility(View.GONE);
                            } else {
                                continueStudyLayout.setVisibility(View.VISIBLE);
                                setContinueStudyAdapter(jsonArray);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("version", version);
        body.put("device_id", sharedHelperModel.getDeviceToken());

        Log.e(TAG, "HomeBody: " + body);

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getHomeChapter, body, headers);
    }

    private void showAppUpdateDialog() {
        Dialog updateDialog = new Dialog(getActivity());
        updateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        updateDialog.setContentView(R.layout.update_app_dialog);
        updateDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        updateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        updateDialog.getWindow().setGravity(Gravity.CENTER);
        updateDialog.setCanceledOnTouchOutside(false);
        updateDialog.setCancelable(false);

        Button updateButton = updateDialog.findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        updateDialog.show();
    }



    private void keyDialogListener(View view) {
        LinearLayout keyDetails = view.findViewById(R.id.keyDetails);
        keyDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyDetailsDialog.showDialog(getActivity());
            }
        });
    }

    private void setSubjectsAdapter(JSONObject response) {
        subjectsAdapter = new SubjectsHomeAdapter(getActivity(), response, subjectsRecyclerView);
        subjectsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        subjectsRecyclerView.setNestedScrollingEnabled(false);
        subjectsRecyclerView.setHasFixedSize(true);
        subjectsRecyclerView.setFocusable(false);
        subjectsRecyclerView.setAdapter(subjectsAdapter);


    }

    private void setContinueStudyAdapter(JSONArray jsonArray) {
        continueStudyAdapter = new ContinueReadingAdapter(getActivity(), jsonArray);
        continueStudyRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        continueStudyRecyclerView.setAdapter(continueStudyAdapter);
    }

    private void initViews(View view) {
        continueStudyRecyclerView = view.findViewById(R.id.continueStudyRecyclerView);
        subjectsRecyclerView = view.findViewById(R.id.subjectsRecyclerView);
        viewPager = view.findViewById(R.id.container);
        continueStudyLayout = view.findViewById(R.id.continueStudyLayout);
        keysText = view.findViewById(R.id.keysText);
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        indicator = view.findViewById(R.id.indicator);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        keyDetails = view.findViewById(R.id.keyDetails);
        downArrow = view.findViewById(R.id.downArrow);
        notification = view.findViewById(R.id.notification);
    }

}