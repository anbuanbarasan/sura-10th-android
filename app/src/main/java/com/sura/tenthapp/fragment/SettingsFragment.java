package com.sura.tenthapp.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.activity.ChooseMedium;
import com.sura.tenthapp.modules.settings.Feedback;
import com.sura.tenthapp.modules.login_register.MobileNumberLogin;
import com.sura.tenthapp.modules.settings.MyProfile;
import com.sura.tenthapp.modules.settings.PaymentDetails;
import com.sura.tenthapp.modules.settings.ReferYourFriend;
import com.sura.tenthapp.modules.settings.WebViewer;
import com.sura.tenthapp.modules.doubts.subject_list_doubts.YourDoubts;
import com.sura.tenthapp.utils.KeyDetailsDialog;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class SettingsFragment extends Fragment {
    private static final String TAG = "SettingsFragment";
    private LinearLayout profileButton, changeMediumButton, referFriendButton,  feedbackButton, yourDoubtsButton,
            disclaimerButton,tAndcButton,privacyPolicyButton,refundCancelPolicyButton,aboutUsButton,contactUsButton
            ,logoutButton,paymentDetailsButton;
    private TextView keysText;
    private LinearLayout keyDetails;
    private ScrollView scrollView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);

        initViews(view);

        keysText.setText(new SharedHelperModel(getActivity()).getKeysCount() + " Keys");

        LinearLayout keyDetails=view.findViewById(R.id.keyDetails);
        if(referral_concept_enabled){
            referFriendButton.setVisibility(View.VISIBLE);

            if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                keyDetails.setVisibility(View.GONE);
            }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                keyDetails.setVisibility(View.VISIBLE);
            }
        }else{
            keyDetails.setVisibility(View.GONE);
            referFriendButton.setVisibility(View.GONE);
        }

        setListeners();

        keyDialogListener(view);

//        scrollViewBottomHide();

        return view;
    }

    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(getActivity());

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);

                loaderDialog.dismiss();
//                SharedHelpers.clearSharedPreferences(getActivity());
                SharedHelperModel sharedHelperModel = new SharedHelperModel(getActivity());
                sharedHelperModel.setLoginStatus("0");
                sharedHelperModel.setLang("");
                sharedHelperModel.setBooleanCheckHome("0");
                sharedHelperModel.setBooleanCheckFiles("0");
                JSONArray jsonArray=new JSONArray();
                sharedHelperModel.setContinueStudyArray(jsonArray.toString());
                Intent intent = new Intent(getActivity(), MobileNumberLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());

        SharedHelperModel sharedHelperModel = new SharedHelperModel(getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.getDataVolley("GETCALL", UrlHelper.logout, headers);
    }

    public void retryDialog(final String error) {
        try {
            final Dialog retryDialog = new Dialog(getActivity());
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            Button retryButton = retryDialog.findViewById(R.id.retryButton);
            TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(error);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();

                    volleyResponse();
                }
            });
            retryDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    private void setListeners() {
        yourDoubtsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), YourDoubts.class);
                startActivity(intent);
            }
        });

        feedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Feedback.class);
                startActivity(intent);
            }
        });


        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MyProfile.class);
                startActivity(intent);
            }
        });

        changeMediumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChooseMedium.class);
                startActivity(intent);
            }
        });

        referFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ReferYourFriend.class);
                startActivity(intent);
            }
        });
        disclaimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), WebViewer.class);
                String url= UrlHelper.getDisclaimer;
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        tAndcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), WebViewer.class);
                String url= UrlHelper.getTermsConditions;
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        privacyPolicyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), WebViewer.class);
                String url= UrlHelper.getPrivacyPolicy;
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        refundCancelPolicyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), WebViewer.class);
                String url= UrlHelper.getRefundPolicy;
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        aboutUsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), WebViewer.class);
                String url= UrlHelper.getAboutUs;
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        contactUsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), WebViewer.class);
                String url= UrlHelper.getContactUs;
                intent.putExtra("url",url);
                startActivity(intent);
            }
        });
        paymentDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), PaymentDetails.class);
                startActivity(intent);
            }
        });
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutDialog(getActivity());
            }
        });
    }

    private void showLogoutDialog(Context context) {
        final Dialog logoutDialog = new Dialog(getActivity());
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setContentView(R.layout.logout_dialog);
        logoutDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        logoutDialog.getWindow().setGravity(Gravity.CENTER);
        Button logoutButton=logoutDialog.findViewById(R.id.logoutButton);
        Button cancelButton=logoutDialog.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutDialog.dismiss();

            }
        });
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutDialog.dismiss();
                volleyResponse();
            }
        });
        logoutDialog.show();
    }

    private void initViews(View view) {
        profileButton = view.findViewById(R.id.profileButton);
        changeMediumButton = view.findViewById(R.id.changeMediumButton);
        referFriendButton = view.findViewById(R.id.referFriendButton);
        feedbackButton = view.findViewById(R.id.feedbackButton);
        yourDoubtsButton = view.findViewById(R.id.yourDoubtsButton);
        keysText = view.findViewById(R.id.keysText);
        keyDetails = view.findViewById(R.id.keyDetails);
        disclaimerButton= view.findViewById(R.id.disclaimerButton);
        tAndcButton= view.findViewById(R.id.tAndcButton);
        privacyPolicyButton= view.findViewById(R.id.privacyPolicyButton);
        refundCancelPolicyButton= view.findViewById(R.id.refundCancelPolicyButton);
        aboutUsButton= view.findViewById(R.id.aboutUsButton);
        contactUsButton= view.findViewById(R.id.contactUsButton);
        logoutButton= view.findViewById(R.id.logoutButton);
        paymentDetailsButton= view.findViewById(R.id.paymentDetailsButton);
        scrollView= view.findViewById(R.id.scrollView);

    }

    private void keyDialogListener(View view) {
        keyDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyDetailsDialog.showDialog(getActivity());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(getActivity()).getKeysCount() + " Keys");
    }
}