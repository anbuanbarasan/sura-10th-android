package com.sura.tenthapp.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sura.tenthapp.R;

public class GetStartedPagerFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section-icon";
    private static final String ARG_SECTION_TITLE = "section-title";
    private static final String ARG_SECTION_DESCRI = "section-descri";

    public static GetStartedPagerFragment newInstance(int getStartedTitle, int getStartedDescri, int icon) {
        GetStartedPagerFragment fragment = new GetStartedPagerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, icon);
        args.putInt(ARG_SECTION_TITLE, getStartedTitle);
        args.putInt(ARG_SECTION_DESCRI, getStartedDescri);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.get_started_pager, container, false);

        ImageView image = rootView.findViewById(R.id.getStartedImage);
        TextView getStartedTitle = rootView.findViewById(R.id.getStartedTitle);
        TextView getStartedDescri = rootView.findViewById(R.id.getStartedDescri);
        image.setImageResource(getArguments().getInt(ARG_SECTION_NUMBER));
        getStartedTitle.setText(getArguments().getInt(ARG_SECTION_TITLE));
        getStartedDescri.setText(getArguments().getInt(ARG_SECTION_DESCRI));
        return rootView;
    }

} 