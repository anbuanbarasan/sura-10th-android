package com.sura.tenthapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.NotificationDetailActivity;
import com.sura.tenthapp.utils.Utils;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>{
    private Context context;
    private JSONArray notification_list=new JSONArray();
    public NotificationAdapter(Context context) {
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(context).load(notification_list.optJSONObject(position).optString("image")).error(R.drawable.logo).into(holder.notification_image);
        holder.title.setText(notification_list.optJSONObject(position).optString("title"));
        holder.time.setText(Utils.changeServerTimeIntoLocalTime(notification_list.optJSONObject(position).optString("created_at")));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, NotificationDetailActivity.class);
                intent.putExtra("id",notification_list.optJSONObject(position).optString("id"));
                context.startActivity(intent);
            }
        });

        if(notification_list.optJSONObject(position).optString("status").equalsIgnoreCase("read")){
            holder.card.setBackgroundColor(Color.WHITE);

        }else if (notification_list.optJSONObject(position).optString("status").equalsIgnoreCase("unread")){
            holder.card.setBackgroundColor(context.getResources().getColor(R.color.buttonColorBlueTransparent));
        }

    }

    @Override
    public int getItemCount() {
        return notification_list.length();
    }

    public void setData(@Nullable JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            notification_list.put(jsonArray.optJSONObject(i));
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title,message,time;
        private ImageView notification_image;
        private LinearLayout card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            time=itemView.findViewById(R.id.time);
            card=itemView.findViewById(R.id.card);
            notification_image=itemView.findViewById(R.id.notification_image);
        }
    }
}
