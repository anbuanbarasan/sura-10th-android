package com.sura.tenthapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.sura.tenthapp.R;

import org.json.JSONArray;

public class StoreLocatorAdapter extends RecyclerView.Adapter<StoreLocatorAdapter.StoreLocationViewHolder >{

    private Context context;
    private JSONArray list;
    public StoreLocatorAdapter(Context context, JSONArray jsonArray) {
        this.context=context;
        this.list=jsonArray;
    }

    @Override
    public StoreLocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_locator,parent,false);
        StoreLocationViewHolder storeLocationViewHolder=new StoreLocationViewHolder(view);
        return storeLocationViewHolder;
    }

    @Override
    public void onBindViewHolder(StoreLocationViewHolder holder, int position) {
        holder.storeName.setText(list.optJSONObject(position).optString("store_name"));
        holder.storePhoneNumber.setText(list.optJSONObject(position).optString("Phone_no"));
        holder.cityName.setText(list.optJSONObject(position).optString("city")+", "+list.optJSONObject(position).optString("district"));
    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    public void updateItems(JSONArray updated) {
        this.list = updated;
        notifyDataSetChanged();
    }

    class StoreLocationViewHolder extends RecyclerView.ViewHolder {
        TextView storePhoneNumber,cityName,storeName;
        public StoreLocationViewHolder(View itemView) {
            super(itemView);
            storePhoneNumber=itemView.findViewById(R.id.storePhoneNumber);
            cityName=itemView.findViewById(R.id.cityName);
            storeName=itemView.findViewById(R.id.storeName);
        }
    }
}
