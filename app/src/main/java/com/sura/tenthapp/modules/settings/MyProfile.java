package com.sura.tenthapp.modules.settings;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.utils.AWS.UploadCallBack;
import com.sura.tenthapp.utils.AWS.UploadImage;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.GetRealPath;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.alhazmy13.gota.Gota;
import net.alhazmy13.gota.GotaResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class MyProfile extends BaseActivity implements Gota.OnRequestPermissionsBack {

    private static final String TAG = "MyProfile";
    private RelativeLayout backButton;
    private EditText nameEdit,schoolEdit,districtEdit,emailEdit;
    private Button updateButton;
    private TextView enterForFreekey1,enterForFreekey2;
    private Spinner selectGender,groupSpinner;
    String[] gender;
    ArrayAdapter<String> gender_adapter;
    private TextView keysText;
    InputFilter filter;
//    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout,imagePickButton;
    private CircleImageView profilePicture;
    private String realPathUrl="",photoUrl="";
    private LinearLayout teacherLayout;
    RadioGroup whoAreYouRadioGroup, selectStandardRadioGroup;
    private RecyclerView setSubjectRecyclerview;
    private RecyclerView.Adapter setSubjectAdapter;
    private JSONArray selectedSubjects;
    private RadioButton studentRadio,teacherRadio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        initViews();
        //setFilter();
        volleyResponse();
        setListeners();

        radioListener();

        LinearLayout keyDetails=findViewById(R.id.keyDetails);
        if(referral_concept_enabled){
            if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("true")){
                keyDetails.setVisibility(View.GONE);
            }else if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("false")){
                keyDetails.setVisibility(View.VISIBLE);
            }

        }else{
            keyDetails.setVisibility(View.GONE);
        }

        gender = new String[]{"Male","Female"};
        gender_adapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, gender);
        selectGender.setAdapter(gender_adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");

    }

    private void setFilter() {

        nameEdit.setFilters(new InputFilter[]{filter});
        schoolEdit.setFilters(new InputFilter[]{filter});
        districtEdit.setFilters(new InputFilter[]{filter});

        filter = new InputFilter() {
            boolean canEnterSpaceName = false;
            boolean canEnterSpaceSchool = false;
            boolean canEnterSpaceDistrict = false;

            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if(nameEdit.getText().toString().equals(""))
                {
                    canEnterSpaceName = false;
                }
                if(schoolEdit.getText().toString().equals(""))
                {
                    canEnterSpaceSchool = false;
                }
                if(districtEdit.getText().toString().equals(""))
                {
                    canEnterSpaceDistrict = false;
                }

                StringBuilder builder = new StringBuilder();

                for (int i = start; i < end; i++) {
                    char currentChar = source.charAt(i);

                    if (Character.isLetterOrDigit(currentChar) || currentChar == '_') {
                        builder.append(currentChar);
                        canEnterSpaceName = true;
                        canEnterSpaceSchool = true;
                        canEnterSpaceDistrict = true;
                    }

                    if(Character.isWhitespace(currentChar) && canEnterSpaceName) {
                        builder.append(currentChar);
                    }if(Character.isWhitespace(currentChar) && canEnterSpaceSchool) {
                        builder.append(currentChar);
                    }if(Character.isWhitespace(currentChar) && canEnterSpaceDistrict) {
                        builder.append(currentChar);
                    }


                }
                return builder.toString();
            }

        };

    }

    public void retryDialog(String error) {
        final Dialog retryDialog = new Dialog(MyProfile.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton=retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();
                volleyResponse();
            }
        });
        retryDialog.show();
    }



    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
               loaderDialog.dismiss();
                //Check error message on response
                if (response.optString("error").equals("true")) {

                    Toast.makeText(MyProfile.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    Log.e(TAG, "notifySuccess: "+response.optJSONObject("view_user").optString("profile_status") );

                    photoUrl = response.optJSONObject("view_user").optString("profile_image");


                    Log.e(TAG, "URL: "+photoUrl );

                    Glide.with(MyProfile.this).load(photoUrl).error(R.drawable.placeholder).into(profilePicture);

                    if(response.optJSONObject("view_user").optString("profile_status").equals("0")){
                        //he did'nt get the keys already in signup & get keys and update button in profile

                        nameEdit.setText(response.optJSONObject("view_user").optString("name"));
                        schoolEdit.setText(response.optJSONObject("view_user").optString("school"));
                        districtEdit.setText(response.optJSONObject("view_user").optString("district"));
                        int spinnerPosition = gender_adapter.getPosition(response.optJSONObject("view_user").optString("gender"));
                        selectGender.setSelection(spinnerPosition);
                        updateButton.setText(getResources().getString(R.string.update_and_getkey));
                        enterForFreekey1.setText(getResources().getString(R.string.enterForFreeKey));
                        enterForFreekey2.setText(getResources().getString(R.string.enterForFreeKey));

                    }else{
                        //he get keys already in signup & update button in profile

                        nameEdit.setText(response.optJSONObject("view_user").optString("name"));
                        schoolEdit.setText(response.optJSONObject("view_user").optString("school"));
                        districtEdit.setText(response.optJSONObject("view_user").optString("district"));
                        int spinnerPosition = gender_adapter.getPosition(response.optJSONObject("view_user").optString("gender"));
                        selectGender.setSelection(spinnerPosition);
                        updateButton.setText(getResources().getString(R.string.submit));
                        enterForFreekey1.setVisibility(View.GONE);
                        enterForFreekey2.setVisibility(View.GONE);
                    }

                    if (response.optJSONObject("view_user").optString("type").equals("teacher")) {

                        emailEdit.setText(response.optJSONObject("view_user").optString("email"));
                        JSONArray jsonArray=new JSONArray();

                        for(int i=0;i<response.optJSONObject("view_user").optJSONArray("tenth").length();i++){

                            JSONObject jsonObject=new JSONObject();

                            if(response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("select_subject").equals("1")){

                                try {
                                    jsonObject.put("subject_id",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_id"));
                                    jsonObject.put("subject_name",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_name"));
                                    jsonObject.put("isSelected","true");
                                } catch (JSONException e) {

                                }
                            }else{
                                try {
                                    jsonObject.put("subject_id",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_id"));
                                    jsonObject.put("subject_name",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_name"));
                                    jsonObject.put("isSelected","false");
                                } catch (JSONException e) {

                                }
                            }
                            jsonArray.put(jsonObject);

                        }

                        Log.e(TAG, "selectedSubjects: "+jsonArray );
                        setSubjectsAdaper(jsonArray);
                        teacherLayout.setVisibility(View.VISIBLE);

                        studentRadio.setChecked(false);
                        teacherRadio.setChecked(true);

                    }else{
                        studentRadio.setChecked(true);
                        teacherRadio.setChecked(false);
                        teacherLayout.setVisibility(View.GONE);
                        JSONArray jsonArray=new JSONArray();

                        for(int i=0;i<response.optJSONObject("view_user").optJSONArray("tenth").length();i++){

                            JSONObject jsonObject=new JSONObject();

                            if(response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("select_subject").equals("1")){

                                try {
                                    jsonObject.put("subject_id",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_id"));
                                    jsonObject.put("subject_name",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_name"));
                                    jsonObject.put("isSelected","true");
                                } catch (JSONException e) {

                                }
                            }else{
                                try {
                                    jsonObject.put("subject_id",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_id"));
                                    jsonObject.put("subject_name",response.optJSONObject("view_user").optJSONArray("tenth").optJSONObject(i).optString("subject_name"));
                                    jsonObject.put("isSelected","false");
                                } catch (JSONException e) {

                                }
                            }
                            jsonArray.put(jsonObject);

                        }

                        Log.e(TAG, "selectedSubjects: "+jsonArray );
                        setSubjectsAdaper(jsonArray);
                    }
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);

        SharedHelperModel sharedHelperModel=new SharedHelperModel(MyProfile.this);
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("standard", getResources().getString(R.string.standard));

        mVolleyService.postDataVolley("GETCALL", UrlHelper.getProfile,body, header);

    }

    private void setSubjectsAdaper(JSONArray jsonArray) {
        setSubjectAdapter = new SubjectListProfileAdapter(this, jsonArray);
        setSubjectRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        setSubjectRecyclerview.setNestedScrollingEnabled(false);
        setSubjectRecyclerview.setHasFixedSize(true);
        setSubjectRecyclerview.setFocusable(false);
        setSubjectRecyclerview.setAdapter(setSubjectAdapter);
    }



    private void setListeners() {

        imagePickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSelectImageClick(view);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyProfile.super.onBackPressed();
            }
        });
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int radioButtonID = whoAreYouRadioGroup.getCheckedRadioButtonId();
                View radioButton = whoAreYouRadioGroup.findViewById(radioButtonID);
                int idx = whoAreYouRadioGroup.indexOfChild(radioButton);

                RadioButton r = (RadioButton) whoAreYouRadioGroup.getChildAt(idx);
                String type = r.getText().toString().toLowerCase();

                if (type.equals("teacher")) {


                    if (nameEdit.getText().toString().trim().length() != 0) {
                        if (schoolEdit.getText().toString().trim().length() != 0) {
                            if (districtEdit.getText().toString().trim().length() != 0) {

                                int Count=0;

                                selectedSubjects=new JSONArray();

                                for(int i = 0; i< SubjectListProfileAdapter.jsonArray.length(); i++){

                                    JSONObject jsonObject=new JSONObject();

                                    if(SubjectListProfileAdapter.jsonArray.optJSONObject(i).optString("isSelected").equals("true")){
                                        Count=Count+1;
                                        try {
                                            jsonObject.put("subject_id",SubjectListProfileAdapter.jsonArray.optJSONObject(i).optString("subject_id"));
                                            selectedSubjects.put(jsonObject);
                                        } catch (JSONException e) {

                                        }
                                    }
                                }

                                Log.e(TAG, "selectedSubjects: "+selectedSubjects );

                                if (Count == 0) {
                                    Toast.makeText(MyProfile.this, "Please Select Subjects", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (realPathUrl.equalsIgnoreCase("")) {
                                        postVolleyResponse(photoUrl, "teacher");
                                    } else {
                                        uploadDp("teacher");
                                    }
                                }
                            } else {
                                districtEdit.setError(getResources().getString(R.string.fillThefield));
                            }
                        } else {
                            schoolEdit.setError(getResources().getString(R.string.fillThefield));
                        }
                    } else {

                        nameEdit.setError(getResources().getString(R.string.fillThefield));

                    }



                } else {

                    if (nameEdit.getText().toString().trim().length() != 0) {
                        if (schoolEdit.getText().toString().trim().length() != 0) {
                            if (districtEdit.getText().toString().trim().length() != 0) {

                                if (realPathUrl.equalsIgnoreCase("")) {
                                    postVolleyResponse(photoUrl, "student");
                                } else {
                                    uploadDp("student");
                                }

                            } else {
                                districtEdit.setError(getResources().getString(R.string.fillThefield));
                            }
                        } else {
                            schoolEdit.setError(getResources().getString(R.string.fillThefield));
                        }
                    } else {
                        nameEdit.setError(getResources().getString(R.string.fillThefield));
                    }
                }

            }
        });

    }

    public void uploadDp(final String type) {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        File displayPicture = new File(realPathUrl);

        UploadImage.uploadFile(MyProfile.this, displayPicture, new UploadCallBack() {
            @Override
            public void result(boolean status, String message) {
                if (status) {
                    Log.e(TAG, "result: " + message);
                    loaderDialog.dismiss();
                    postVolleyResponse(message, type);

                }
            }
        });

    }
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }


    private void setPermission() {
        new Gota.Builder(this)
                .withPermissions(Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .requestId(1)
                .setListener(MyProfile.this)
                .check();
    }

    @Override
    public void onRequestBack(int requestId, @NonNull GotaResponse gotaResponse) {
        if(gotaResponse.isGranted(Manifest.permission.READ_SMS)) {

        }
//        else{
//            Toast.makeText(this, "Please Enable Permission", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent();
//            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//            Uri uri = Uri.fromParts("package", getPackageName(), null);
//            intent.setData(uri);
//            startActivity(intent);
//        }
    }


    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                setPermission();
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //((ImageButton) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
                realPathUrl= GetRealPath.getRealPathFromUri(this,result.getUri());
                Glide.with(this).load(realPathUrl).into(profilePicture);
                Log.d(TAG, "onActivityResult: "+realPathUrl );
                //Toast.makeText(this, "Cropping successful, Sample: " + url, Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setMultiTouchEnabled(true)
                .setAspectRatio(10,10)
                .start(this);
    }


    private void postVolleyResponse(String message, String type) {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

            IResult mResultCallback = new IResult() {

                @Override
                public void notifySuccess(String requestType, JSONObject response) {
                    Log.e(TAG, "Volley requester " + requestType);
                    Log.e(TAG, "Volley JSON post" + response);
                    loaderDialog.dismiss();
                    //Check error message on response
                    if (response.optString("error").equals("true")) {

                        Toast.makeText(MyProfile.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(MyProfile.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                        new SharedHelperModel(MyProfile.this).setKeysCount(response.optString("keys"));
                        new SharedHelperModel(MyProfile.this).setLoginStudentOrTeacher(response.optString("type"));

                        MyProfile.super.onBackPressed();
                    }
                }

                @Override
                public void notifySuccessString(String requestType, String response) {

                }

                @Override
                public void notifyError(String requestType, String error) {
                    Log.e(TAG, "Volley requester " + error);
                    Log.e(TAG, "Volley JSON post" + "That didn't work!");
                    loaderDialog.dismiss();
                }
            };

            VolleyService mVolleyService = new VolleyService(mResultCallback, this);

            HashMap<String, String> body = new HashMap<String, String>();
            body.put("name", nameEdit.getText().toString().trim());
            body.put("gender", selectGender.getSelectedItem().toString().trim());
            body.put("school", schoolEdit.getText().toString().trim());
            body.put("district", districtEdit.getText().toString().trim());
            body.put("email", emailEdit.getText().toString().trim());
            body.put("profile_image", message);
            body.put("type",type);
            if (type.equals("teacher")) {
                body.put("standard_teacher", "10");
                body.put("subject", selectedSubjects.toString());
            }

            SharedHelperModel sharedHelperModel=new SharedHelperModel(MyProfile.this);
            HashMap<String, String> header = new HashMap<String, String>();
            header.put("authorization", sharedHelperModel.getAccessToken());

            mVolleyService.postDataVolley("POSTCALL", UrlHelper.postProfile,body,header);
    }

    private void radioListener() {

        whoAreYouRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioButton = radioGroup.findViewById(i);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    Log.e(TAG, "onCheckedChanged: " + checkedRadioButton.getText());

                } else {
                    Log.e(TAG, "onCheckedChanged: " + checkedRadioButton.getText());
                }

                if (checkedRadioButton.getText().toString().toLowerCase().equals("teacher")) {

//                    getResponse();

                    teacherLayout.setVisibility(View.VISIBLE);
                }else{
                    teacherLayout.setVisibility(View.GONE);

                }

            }
        });


    }


    private void initViews() {
        backButton=findViewById(R.id.backButton);
        nameEdit=findViewById(R.id.nameEdit);
        schoolEdit=findViewById(R.id.schoolEdit);
        districtEdit=findViewById(R.id.districtEdit);
        updateButton=findViewById(R.id.updateButton);
        enterForFreekey1=findViewById(R.id.enterForFreekey1);
        enterForFreekey2=findViewById(R.id.enterForFreekey2);
        selectGender=findViewById(R.id.selectGender);
        keysText=findViewById(R.id.keysText);
        imagePickButton=findViewById(R.id.imagePickButton);
        profilePicture=findViewById(R.id.profilePicture);
        teacherLayout = findViewById(R.id.teacherLayout);
        whoAreYouRadioGroup = findViewById(R.id.whoAreYouRadioGroup);
        selectStandardRadioGroup = findViewById(R.id.selectStandardRadioGroup);
        groupSpinner = findViewById(R.id.groupSpinner);
        emailEdit = findViewById(R.id.emailEdit);
        setSubjectRecyclerview = findViewById(R.id.subjectsRecyclerView);
        studentRadio= findViewById(R.id.studentRadio);
        teacherRadio= findViewById(R.id.teacherRadio);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
