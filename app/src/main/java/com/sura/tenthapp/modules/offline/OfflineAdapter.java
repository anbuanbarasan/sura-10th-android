package com.sura.tenthapp.modules.offline;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.reading.chapter_contents.BreadCrumbs;
import com.sura.tenthapp.utils.Database;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import static android.content.ContentValues.TAG;

public class OfflineAdapter extends RecyclerView.Adapter<OfflineAdapter.ViewHolder>{

    public Context context;
    public JSONArray jsonArray;
    public Boolean isOffline;
    private Database mHelper;

    public OfflineAdapter(Context activity, JSONArray response, Boolean isOffline) {
        this.context = activity;
        this.jsonArray = response;
        this.isOffline = isOffline;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.favourite_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.subjectNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("subject_division_name"), Html.FROM_HTML_MODE_LEGACY));
            holder.chapterNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("chapter_name"), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.subjectNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("subject_division_name")));
            holder.chapterNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("chapter_name")));
        }

//        holder.subjectNameText.setText(jsonArray.optJSONObject(position).optString("subject_division_name"));
//            holder.chapterNameText.setText(jsonArray.optJSONObject(position).optString("chapter_name"));
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView subjectNameText,chapterNameText;
        public RelativeLayout chapterFavButton,deleteButton;
        public ViewHolder(View itemView) {
            super(itemView);
            mHelper = new Database(context);
            subjectNameText=itemView.findViewById(R.id.subjectNameText);
            chapterNameText=itemView.findViewById(R.id.chapterNameText);
            chapterFavButton=itemView.findViewById(R.id.chapterFavButton);
            deleteButton=itemView.findViewById(R.id.deleteButton);
            chapterFavButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isOffline) {

                        String jsonString = readFromFile(jsonArray.optJSONObject(getAdapterPosition()).optString("file_location"));
                        try {
                            Log.e(TAG, "onClick: " + jsonString);
                            Intent i = new Intent(context, BreadCrumbs.class);
                            i.putExtra("offlineArray", jsonString);
                            i.putExtra("isOffline", "true");
                            i.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
                            i.putExtra("chapterName", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_name"));
                            context.startActivity(i);
                        } catch (Exception e) {

                        }


                    } else {

                        Intent i = new Intent(context, BreadCrumbs.class);
                        i.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
                        i.putExtra("chapterName", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_name"));
                        i.putExtra("chapterNumber", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_number"));
                        i.putExtra("isOffline", "false");
                        context.startActivity(i);
                    }

                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDeleteDialog(jsonArray.optJSONObject(getAdapterPosition()).optString("file_location"), jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
                }
            });
        }
    }

    private void showDeleteDialog(String file_location, final String id) {

        final Dialog deleteDialog = new Dialog(context);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteDialog.setContentView(R.layout.delete_offline_dialog);
        deleteDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//      loaderDialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.color.transparentWhite));
        deleteDialog.getWindow().setGravity(Gravity.CENTER);
        deleteDialog.setCanceledOnTouchOutside(false);
        deleteDialog.setCancelable(false);
        deleteDialog.show();

        Button confirmButton = deleteDialog.findViewById(R.id.confirmButton);
        Button cancelButton = deleteDialog.findViewById(R.id.cancelButton);

        final String location = file_location.replaceAll("/chapterContent.json", "");

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteDialog.dismiss();

                File dir = new File(location);
                if (dir.isDirectory()) {
                    String[] children = dir.list();
                    Log.e(TAG, "onClick: " + children.length);
                    for (int i = 0; i < children.length; i++) {
                        new File(dir, children[i]).delete();
                    }
                    if (mHelper.deleteTitle(id)) {
                        jsonArray=mHelper.selectFromDatabase();
                        notifyDataSetChanged();
                        Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Try Again Later", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Log.e(TAG, "onClick: ");
                }

            }

        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });


    }

    private String readFromFile(String path) {

        String ret = "";

        try {
//            InputStream inputStream = context.openFileInput(path);

            FileInputStream inputStream = new FileInputStream(new File(path));

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }

            inputStream.close();
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }


}