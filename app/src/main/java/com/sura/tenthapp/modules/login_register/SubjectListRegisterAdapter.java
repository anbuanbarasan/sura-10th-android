package com.sura.tenthapp.modules.login_register;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SubjectListRegisterAdapter extends RecyclerView.Adapter<SubjectListRegisterAdapter.ViewHolder> {

    private static final String TAG = SubjectListRegisterAdapter.class.getSimpleName();
    public Context context;
    private List<Register.Subjects> subjects;
    //public static JSONArray selectedJsonArray;

    public SubjectListRegisterAdapter(Context context, List<Register.Subjects> subjects) {
        this.subjects = subjects;
        this.context = context;
        //selectedJsonArray = new JSONArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.set_subject_register_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.subjectName.setText(subjects.get(position).getSubjectName());

        if (subjects.get(position).isSelected()) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {

        return subjects.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView subjectName;
        public LinearLayout clickSubject;
        public CheckBox checkbox;

        public ViewHolder(View itemView) {
            super(itemView);

            subjectName = itemView.findViewById(R.id.subjectName);
            clickSubject = itemView.findViewById(R.id.clickSubject);
            checkbox = itemView.findViewById(R.id.checkbox);

            this.setIsRecyclable(false);


            clickSubject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final int position=getAdapterPosition();

                    Log.e(TAG, "position: "+position );

                    boolean isSelcted = subjects.get(position).isSelected();
                    subjects.get(position).setSelected(!isSelcted);

                    notifyDataSetChanged();

//                if (holder.checkbox.isChecked()) {
//                    holder.checkbox.setChecked(false);
//                    int pos = holder.getAdapterPosition();
//
//                    String subjectId = jsonArray.optJSONObject(pos).optString("subject_id");
//
//                    for (int i = 0; i < selectedJsonArray.length(); i++) {
//
//                        if (subjectId.equals(selectedJsonArray.optJSONObject(i).optString("subject_id"))) {
//                            selectedJsonArray = remove(i, selectedJsonArray);
//                        }
//                    }
//
//                    Log.e(TAG, "deleteSelectedJsonArray: " + selectedJsonArray);
//
//
//                } else {
//
//                    holder.checkbox.setChecked(true);
//                    int pos = holder.getAdapterPosition();
//
//                    JSONObject jsonObject = new JSONObject();
//
//                    try {
//                        jsonObject.put("subject_id", jsonArray.optJSONObject(pos).optString("subject_id"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        e.getMessage();
//                    }
//
//                    selectedJsonArray.put(jsonObject);
//                    Log.e(TAG, "selectedJsonArray: " + selectedJsonArray);
//
//                }
                }
            });

        }
    }

    public static JSONArray remove(final int index, final JSONArray from) {
        final List<JSONObject> objs = getList(from);
        objs.remove(index);

        final JSONArray jarray = new JSONArray();
        for (final JSONObject obj : objs) {
            jarray.put(obj);
        }


        return jarray;
    }

    public static List<JSONObject> getList(final JSONArray jarray) {
        final int len = jarray.length();
        final ArrayList<JSONObject> result = new ArrayList<JSONObject>(len);
        for (int i = 0; i < len; i++) {
            final JSONObject obj = jarray.optJSONObject(i);
            if (obj != null) {
                result.add(obj);
            }
        }
        return result;
    }
}