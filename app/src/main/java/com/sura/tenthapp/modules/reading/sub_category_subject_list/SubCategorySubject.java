package com.sura.tenthapp.modules.reading.sub_category_subject_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import com.google.android.gms.ads.AdView;
import com.facebook.internal.LockOnGetVariable;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.SharedHelperModel;

import org.json.JSONArray;
import org.json.JSONException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class SubCategorySubject extends BaseActivity {

    private static final String TAG = SubCategorySubject.class.getSimpleName();
    private RecyclerView subCatetorySubjectRecyclerview;
    private RecyclerView.Adapter subCategorySubjectAdapter;
    private Intent intent;
    private RelativeLayout backButton, adLayout;
    private TextView keysText;
    private TextView chapterNameText;
//    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String subjectId, subjectName, subjectCost, subjectPaymentId, subjectImageName;
    private LinearLayoutManager linearLayoutManager;
    private ImageView downArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_category_subject);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.SUB_CATEGORY_SUBJECT_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });

        LinearLayout keyDetails = findViewById(R.id.keyDetails);
        if (referral_concept_enabled) {

            if (new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("true")) {
                keyDetails.setVisibility(View.GONE);
            } else if (new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("false")) {
                keyDetails.setVisibility(View.VISIBLE);
            }
        } else {
            keyDetails.setVisibility(View.GONE);
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);


        initViews();
        intent = getIntent();
        String extra = intent.getStringExtra("subcategory");
        subjectId = intent.getStringExtra("subjectId");
        subjectName = intent.getStringExtra("subjectName");
        subjectCost = intent.getStringExtra("subjectCost");
        subjectPaymentId = intent.getStringExtra("subjectPaymentId");
        subjectImageName = intent.getStringExtra("subjectImageName");
        chapterNameText.setText(subjectName);

        try {
            JSONArray jsonArray = new JSONArray(extra);
            setSubCategorySubjectAdapter(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubCategorySubject.super.onBackPressed();
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");
    }

    private void setSubCategorySubjectAdapter(final JSONArray jsonArray) {
        linearLayoutManager=new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        subCategorySubjectAdapter = new SubCategorySubjectAdapter(this, jsonArray, subjectId, subjectCost, subjectPaymentId, subjectName, subjectImageName);
        subCatetorySubjectRecyclerview.setLayoutManager(linearLayoutManager);
        subCatetorySubjectRecyclerview.setAdapter(subCategorySubjectAdapter);

        subCatetorySubjectRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int total = linearLayoutManager.getItemCount();
                int firstVisibleItemCount = linearLayoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemCount = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                Log.e(TAG, "onScrolled: "+lastVisibleItemCount );
                if(lastVisibleItemCount==total-1){
                    downArrow.setVisibility(View.GONE);
                }else{
                    downArrow.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    private void initViews() {
        subCatetorySubjectRecyclerview = findViewById(R.id.subCategorySubjectRecyclerView);
        backButton = findViewById(R.id.backButton);
        keysText = findViewById(R.id.keysText);
        chapterNameText = findViewById(R.id.chapterNameText);
        downArrow = findViewById(R.id.downArrow);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
