package com.sura.tenthapp.modules.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiBanner;
import com.inmobi.sdk.InMobiSdk;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.PlacementId;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class ReferYourFriend extends BaseActivity {

    private static final String TAG = "ReferYourFriend";
    private TextView referalCode;
    private LinearLayout referalCodeLayout,shareCode;
    private EditText referalCodeEdit1,referalCodeEdit2,referalCodeEdit3,referalCodeEdit4,referalCodeEdit5,referalCodeEdit6;
    private Button submitButton;
    private TextView keysText;
    private RelativeLayout backButton;
//    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refer_your_friend);
//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        /*InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);

        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
        RelativeLayout adContainer = findViewById(R.id.ad_container);
        float density = getResources().getDisplayMetrics().density;
        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
        adContainer.addView(bannerAd, bannerLp);
        bannerAd.load();

        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
            @Override
            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onAdLoadSucceeded");
            }

            @Override
            public void onAdLoadFailed(InMobiBanner inMobiBanner,
                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
                Log.d(TAG, "Banner ad failed to load with error: " +
                        inMobiAdRequestStatus.getMessage());
            }

            @Override
            public void onAdDisplayed(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onAdDisplayed");
            }

            @Override
            public void onAdDismissed(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onAdDismissed");
            }

            @Override
            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
                Log.d(TAG, "onAdInteraction");
            }

            @Override
            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onUserLeftApplication");
            }

            @Override
            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
                Log.d(TAG, "onAdRewardActionCompleted");
            }
        });*/

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        LinearLayout keyDetails=findViewById(R.id.keyDetails);
        if(referral_concept_enabled){

            if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("true")){
                keyDetails.setVisibility(View.GONE);
            }else if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("false")){
                keyDetails.setVisibility(View.VISIBLE);
            }
        }else{
            keyDetails.setVisibility(View.GONE);
        }

        initViews();
        setReferalText();
        showReferalLayout();
        textWatcher();
        setListeners();
        backPressedEditText();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void backPressedEditText() {
        referalCodeEdit1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                }
                return false;
            }
        });
        referalCodeEdit2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    referalCodeEdit1.requestFocus();
                }
                return false;
            }
        });
        referalCodeEdit3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    referalCodeEdit2.requestFocus();
                }
                return false;
            }
        });
        referalCodeEdit4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    referalCodeEdit3.requestFocus();
                }
                return false;
            }
        });
        referalCodeEdit5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    referalCodeEdit4.requestFocus();
                }
                return false;
            }
        });
        referalCodeEdit6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    referalCodeEdit5.requestFocus();
                }
                return false;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");

    }

    public void retryDialog(final String error) {
        final Dialog retryDialog = new Dialog(ReferYourFriend.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton=retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();

                postVolleyResponse();

            }
        });
        retryDialog.show();
    }


    private void setListeners() {

        shareCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Refer and Earn keys points \n\nKey : "+new SharedHelperModel(ReferYourFriend.this).getUserReferalCode());
//                sendIntent.setType("text/plain");
//                startActivity(sendIntent);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String appLink="https://play.google.com/store/apps/details?id=com.sura.tenthapp";
                String referalCode=new SharedHelperModel(ReferYourFriend.this).getUserReferalCode();
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Download Sura 10th App from Play Store, Refer your friend and Earn keys points \n\nKey : "+referalCode+"\n\nDownload Link:\n"+appLink);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share to.."));
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReferYourFriend.super.onBackPressed();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(referalCodeEdit1.getText().toString().length()!=0||referalCodeEdit2.getText().toString().length()!=0||
                        referalCodeEdit3.getText().toString().length()!=0||referalCodeEdit4.getText().toString().length()!=0
                        ||referalCodeEdit5.getText().toString().length()!=0){
                    postVolleyResponse();
                }else{
                    Toast.makeText(ReferYourFriend.this, "Please Fill the Field", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void postVolleyResponse() {
        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if(response.optString("error").equals("false")){
                    Toast.makeText(ReferYourFriend.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    new SharedHelperModel(ReferYourFriend.this).setReferalCodeStatus("1");
                    new SharedHelperModel(ReferYourFriend.this).setKeysCount(response.optString("keys_no"));
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                    referalCodeLayout.setVisibility(View.GONE);
                }else{
                    Toast.makeText(ReferYourFriend.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType,String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
               loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback,this);
        SharedHelperModel sharedHelperModel=new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization",sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("referral_code",referalCodeEdit1.getText().toString()+referalCodeEdit2.getText().toString()+referalCodeEdit3.getText().toString()+
                referalCodeEdit4.getText().toString()+referalCodeEdit5.getText().toString()+referalCodeEdit6.getText().toString());

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postReferalCode,body,headers);
    }

    private void textWatcher() {
        referalCodeEdit1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(referalCodeEdit1.getText().toString().length()==1)
                {
                    referalCodeEdit2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        referalCodeEdit2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(referalCodeEdit2.getText().toString().length()==1)
                {
                    referalCodeEdit3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        referalCodeEdit3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(referalCodeEdit3.getText().toString().length()==1)
                {
                    referalCodeEdit4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        referalCodeEdit4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(referalCodeEdit4.getText().toString().length()==1)
                {
                    referalCodeEdit5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        referalCodeEdit5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(referalCodeEdit5.getText().toString().length()==1)
                {
                    referalCodeEdit6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        referalCodeEdit6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(referalCodeEdit6.getText().toString().length()==1)
                {
                    referalCodeEdit6.clearFocus();
                    hideSoftKeyboard();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    private void showReferalLayout() {
        if(new SharedHelperModel(ReferYourFriend.this).getReferalCodeStatus().equals("1")){
            referalCodeLayout.setVisibility(View.GONE);
        }else{
            referalCodeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setReferalText() {
        referalCode.setText(new SharedHelperModel(ReferYourFriend.this).getUserReferalCode());
    }

    private void initViews() {
        referalCode=findViewById(R.id.referalCode);
        referalCodeLayout=findViewById(R.id.referalCodeLayout);
        referalCodeEdit1=findViewById(R.id.referalCodeEdit1);
        referalCodeEdit2=findViewById(R.id.referalCodeEdit2);
        referalCodeEdit3=findViewById(R.id.referalCodeEdit3);
        referalCodeEdit4=findViewById(R.id.referalCodeEdit4);
        referalCodeEdit5=findViewById(R.id.referalCodeEdit5);
        referalCodeEdit6=findViewById(R.id.referalCodeEdit6);
        submitButton=findViewById(R.id.submitButton);
        keysText=findViewById(R.id.keysText);
        backButton=findViewById(R.id.backButton);
        shareCode=findViewById(R.id.shareCode);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
