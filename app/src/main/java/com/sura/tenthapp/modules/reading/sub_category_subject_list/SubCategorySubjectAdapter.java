package com.sura.tenthapp.modules.reading.sub_category_subject_list;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.reading.chapter_list.ChapterList;

import org.json.JSONArray;

public class SubCategorySubjectAdapter extends RecyclerView.Adapter<SubCategorySubjectAdapter.ViewHolder>{

    private static final String TAG = SubCategorySubjectAdapter.class.getSimpleName();
    Context  context;
    public JSONArray jsonArray;
    public String subjectId = "", subjectCost = "", subjectPaymentId = "", subjectName = "", subjectImageName = "";
    public SubCategorySubjectAdapter(Context subCategorySubject, JSONArray jsonArray, String subjectId, String subjectCost, String subjectPaymentId, String subjectName, String subjectImageName) {
        this.context=subCategorySubject;
        this.jsonArray=jsonArray;
        this.subjectId=subjectId;
        this.subjectCost=subjectCost;
        this.subjectPaymentId=subjectPaymentId;
        this.subjectName = subjectName;
        this.subjectImageName = subjectImageName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_category_subjects_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.subjectText.setText(jsonArray.optJSONObject(position).optString("subject_division_name"));

        if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("physics")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.physicsColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.physics));
        } else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("chemistry")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.chemistryColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.chemistry));
        } else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("biology")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.biologyColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.bio));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("civics")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.civisColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.civics));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("economics")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.economicsColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.economics));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("history")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.historyColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.history));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("geography")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.geographyColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.geo));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("english")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.englishColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.english));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("tamil")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.tamilColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.tamil));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("maths")){
            holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.mathsColor));
            holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.maths));
        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public CardView subjectButton;
        public TextView subjectText;
        private ImageView subjectIcon;
        public ViewHolder(View itemView) {
            super(itemView);
            subjectButton=itemView.findViewById(R.id.subjectButton);
            subjectText=itemView.findViewById(R.id.subjectText);
            subjectIcon=itemView.findViewById(R.id.subjectIcon);
            subjectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context , ChapterList.class);
                    Log.e("Log", "onClick: "+ jsonArray.optJSONObject(getAdapterPosition()).optString("subject_id"));
                    intent.putExtra("subjectDivisionId",jsonArray.optJSONObject(getAdapterPosition()).optString("subject_division_id"));
                    intent.putExtra("subjectName",jsonArray.optJSONObject(getAdapterPosition()).optString("subject_division_name"));
                    intent.putExtra("subjectId",subjectId);
                    intent.putExtra("subjectCost",subjectCost);
                    intent.putExtra("subjectPaymentId",subjectPaymentId);
                    intent.putExtra("subjectImageName",subjectImageName);
                    intent.putExtra("subjectDivisionName",jsonArray.optJSONObject(getAdapterPosition()).optString("subject_division_name"));
                    intent.putExtra("subjectDivisionImageName",jsonArray.optJSONObject(getAdapterPosition()).optString("subject_division_image_name"));
                    intent.putExtra("from","subCategory");
                    context.startActivity(intent);
                }
            });
        }
    }
}