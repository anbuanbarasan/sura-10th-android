package com.sura.tenthapp.modules.doubts.answer_lists;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class AnswerPerSubjectAdapter extends RecyclerView.Adapter<AnswerPerSubjectAdapter.ViewHolder> {
    private static final String TAG = AnswerPerSubjectAdapter.class.getSimpleName();
    public Context context;
    public JSONArray jsonArray;

    public AnswerPerSubjectAdapter(Context context, JSONArray jsonArray) {

        this.context = context;
        this.jsonArray = jsonArray;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.answerpersubject_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0) {
            holder.answerLabel.setVisibility(View.VISIBLE);
        } else {
            holder.answerLabel.setVisibility(View.INVISIBLE);
        }
        holder.answerText.setText(jsonArray.optJSONObject(position).optString("answer"));
        holder.usefullText.setText("Usefull (" + jsonArray.optJSONObject(position).optString("ups_count") + ")");
        holder.unUsefulText.setText("Not Usefull (" + jsonArray.optJSONObject(position).optString("downs_count") + ")");
        holder.answeredOnText.setText("Answered on : " + convertDateFormat(jsonArray.optJSONObject(position).optString("created_at")).substring(0, 11));
        holder.answeredByText.setText("Answered by : "+jsonArray.optJSONObject(position).optString("type"));


        if (jsonArray.optJSONObject(position).optString("ups").equals("0")) {
            holder.useFullImage.setImageResource(R.drawable.like_unfilled);
        } else {
            holder.useFullImage.setImageResource(R.drawable.like_filled);
        }


//        if(jsonArray.optJSONObject(position).optString("ups").equalsIgnoreCase("1")){
//            holder.useFullImage.setImageDrawable(context.getResources().getDrawable(R.drawable.like_filled));
//        }

        if (jsonArray.optJSONObject(position).optString("downs").equalsIgnoreCase("0")) {
            holder.notUseFullImage.setImageResource(R.drawable.dislike_unfilled);
        } else {
            holder.notUseFullImage.setImageResource(R.drawable.dislike_filled);
        }
//        if(jsonArray.optJSONObject(position).optString("downs").equalsIgnoreCase("1")){
//            holder.notUseFullImage.setImageResource(R.drawable.dislike_filled);
//        }

        String imageString=jsonArray.optJSONObject(position).optString("images");
        String pdfString=jsonArray.optJSONObject(position).optString("pdf");
        try {
            JSONArray imageArray=new JSONArray(imageString);
            if(imageArray.length()==0){
                holder.viewImageButton.setVisibility(View.GONE);
            }else{
                holder.viewImageButton.setVisibility(View.VISIBLE);
            }
            JSONArray pdfArray=new JSONArray(pdfString);
            if(pdfArray.length()==0){
                holder.viewPdfButton.setVisibility(View.GONE);
            }else{
                holder.viewPdfButton.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {

        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView answerText, unUsefulText, usefullText, answerLabel, answeredOnText,answeredByText;
        public LinearLayout useFullLayout, unusefullLayout;
        public ImageView notUseFullImage, useFullImage;
        public RelativeLayout viewImageButton,viewPdfButton;

        public ViewHolder(View itemView) {
            super(itemView);
            answerText = itemView.findViewById(R.id.answerText);
            unUsefulText = itemView.findViewById(R.id.unUsefulText);
            usefullText = itemView.findViewById(R.id.usefullText);
            answerLabel = itemView.findViewById(R.id.answerLabel);
            useFullLayout = itemView.findViewById(R.id.useFullLayout);
            unusefullLayout = itemView.findViewById(R.id.unusefullLayout);
            useFullImage = itemView.findViewById(R.id.useFullImage);
            notUseFullImage = itemView.findViewById(R.id.notUseFullImage);
            answeredOnText = itemView.findViewById(R.id.answeredOnText);
            answeredByText = itemView.findViewById(R.id.answeredByText);
            viewImageButton = itemView.findViewById(R.id.viewImageButton);
            viewPdfButton = itemView.findViewById(R.id.viewPdfButton);


            viewImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();

                    Intent intent=new Intent(context,AnswersImageViewer.class);
                    intent.putExtra("from","image");
                    intent.putExtra("imageObject",jsonArray.optJSONObject(pos).optString("images"));
                    context.startActivity(intent);
                }
            });

            viewPdfButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();

                    Intent intent=new Intent(context,AnswersImageViewer.class);
                    intent.putExtra("from","pdf");
                    intent.putExtra("pdfObject",jsonArray.optJSONObject(pos).optString("pdf"));
                    context.startActivity(intent);
                }
            });

            useFullLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos = getAdapterPosition();

                    if (jsonArray.optJSONObject(getAdapterPosition()).optString("downs").equals("0")) {

                        if (jsonArray.optJSONObject(getAdapterPosition()).optString("ups").equals("1")) {

                            try {
                                jsonArray.optJSONObject(pos).put("ups", "0");
                                int count= Integer.parseInt(jsonArray.optJSONObject(pos).optString("ups_count"));
                                jsonArray.optJSONObject(pos).put("ups_count",String.valueOf(count-1));
                            } catch (JSONException e) {

                            }
                            Log.e(TAG, "onClick: " + jsonArray);
                            notifyDataSetChanged();
                            likeAndDislikeResponse(jsonArray.optJSONObject(pos).optString("id"), "ups", "remove");

                        } else {

                            try {
                                jsonArray.optJSONObject(pos).put("ups", "1");
                                int count= Integer.parseInt(jsonArray.optJSONObject(pos).optString("ups_count"));
                                jsonArray.optJSONObject(pos).put("ups_count",String.valueOf(count+1));
                            } catch (JSONException e) {

                            }
                            Log.e(TAG, "onClick: " + jsonArray);
                            notifyDataSetChanged();
                            likeAndDislikeResponse(jsonArray.optJSONObject(pos).optString("id"), "ups", "add");

                        }

                    }

                }
            });

            unusefullLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos = getAdapterPosition();

                    if (jsonArray.optJSONObject(getAdapterPosition()).optString("ups").equals("0")) {

                        if (jsonArray.optJSONObject(getAdapterPosition()).optString("downs").equals("1")) {

                            try {
                                jsonArray.optJSONObject(pos).put("downs", "0");
                                int count= Integer.parseInt(jsonArray.optJSONObject(pos).optString("downs_count"));
                                jsonArray.optJSONObject(pos).put("downs_count",String.valueOf(count-1));
                            } catch (JSONException e) {

                            }
                            Log.e(TAG, "onClick: " + jsonArray);
                            likeAndDislikeResponse(jsonArray.optJSONObject(pos).optString("id"), "downs", "remove");
                            notifyDataSetChanged();
                        } else {

                            try {
                                jsonArray.optJSONObject(pos).put("downs", "1");
                                int count= Integer.parseInt(jsonArray.optJSONObject(pos).optString("downs_count"));
                                jsonArray.optJSONObject(pos).put("downs_count",String.valueOf(count+1));
                            } catch (JSONException e) {

                            }
                            Log.e(TAG, "onClick: " + jsonArray);
                            notifyDataSetChanged();
                            likeAndDislikeResponse(jsonArray.optJSONObject(pos).optString("id"), "downs", "add");

                        }

                    }


                }
            });
        }
    }

    private void likeAndDislikeResponse(String answerId, String upsDown, String action) {

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("answer_id", answerId);
        body.put("action", action);
        body.put("ups_down", upsDown);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postLikeDislike, body, headers);
    }

    public String convertDateFormat(String dateFormat) {

        Log.e(TAG, "convertDateFormat: " + dateFormat);
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateFormat);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}