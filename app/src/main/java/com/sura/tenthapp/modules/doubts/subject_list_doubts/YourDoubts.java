package com.sura.tenthapp.modules.doubts.subject_list_doubts;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.doubts.doubt_lists.DoubtListPerSubject;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class YourDoubts extends BaseActivity {

    private static final String TAG = "YourDoubts";
    private RecyclerView yourDoubtsRecyclerview;
    private RecyclerView.Adapter yourDoubtsAdapter;
    private RelativeLayout backButton;
//    private AdView mAdView;
    private TextView noDoubtsText;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout,viewYourDoubts;
    private TextView keysText;
    private LinearLayout yourDoubtsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_doubts);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });


        noDoubtsText = findViewById(R.id.noDoubtsText);
        yourDoubtsButton = findViewById(R.id.yourDoubtsButton);
        viewYourDoubts = findViewById(R.id.viewYourDoubts);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

//        mAdView.setAdListener(new AdListener(){
//            @Override
//            public void onAdLoaded() {
//                adLayout.setVisibility(View.VISIBLE);
//            }
//        });

        yourDoubtsRecyclerview = findViewById(R.id.yourDoubtsRecyclerview);
        backButton = findViewById(R.id.backButton);

        if(new SharedHelperModel(YourDoubts.this).getLoginStudentOrTeacher().equals("teacher")){

            viewYourDoubts.setVisibility(View.GONE);

        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YourDoubts.super.onBackPressed();
            }
        });

        getDoubtsVolleyResponse();

        yourDoubtsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(YourDoubts.this,DoubtListPerSubject.class);
                intent.putExtra("from","yourDoubts");
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");
//    }

    private void getDoubtsVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(YourDoubts.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    if (response.optString("error_message").equals("No list.")) {
                        noDoubtsText.setVisibility(View.VISIBLE);
//                        Toast.makeText(YourDoubts.this, "No Dobts are there", Toast.LENGTH_SHORT).show();

                    } else {
                        noDoubtsText.setVisibility(View.GONE);
                        setYourDoubtsAdapter(response.optJSONArray("list_subject"));
                    }
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        if(new SharedHelperModel(YourDoubts.this).getLoginStudentOrTeacher().equals("teacher")){
            mVolleyService.getDataVolley("POSTCALL", UrlHelper.getTeacherDoubtSubjects,  headers);

        }else{
            mVolleyService.postDataVolley("POSTCALL", UrlHelper.getHomeChapter, body, headers);
        }


    }

    private void setYourDoubtsAdapter(JSONArray jsonArray) {

        yourDoubtsAdapter = new YourDoubtsAdapter(this, jsonArray);
        yourDoubtsRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        yourDoubtsRecyclerview.setAdapter(yourDoubtsAdapter);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
