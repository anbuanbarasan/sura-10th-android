package com.sura.tenthapp.modules.login_register;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.adapter.PagerAdapter;
import com.sura.tenthapp.fragment.GetStartedPagerFragment;
import com.sura.tenthapp.utils.language.BaseActivity;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GetStarted extends BaseActivity {

    private  PagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    private Button getStartedButton;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_started);

        initViews();
        setListeners();

        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());

        mPagerAdapter.addFragment(GetStartedPagerFragment.newInstance(R.string.getStartedFirstTitle,R.string.getStartedFirstDescri, R.drawable.get_started_image_two));
        mPagerAdapter.addFragment(GetStartedPagerFragment.newInstance(R.string.getStartedSecondTitle,R.string.getStartedSecondDescri, R.drawable.get_started_image_three));
        mPagerAdapter.addFragment(GetStartedPagerFragment.newInstance(R.string.learnMore,R.string.scoreHighMarks, R.drawable.get_started_image));

        mViewPager = findViewById(R.id.container);
        CircleIndicator indicator = findViewById(R.id.indicator);
        mViewPager.setAdapter(mPagerAdapter);
        indicator.setViewPager(mViewPager);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

    }

    private void setListeners() {
        getStartedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(GetStarted.this,MobileNumberLogin.class);
                startActivity(intent);
            }
        });
    }

    private void initViews() {
        getStartedButton=findViewById(R.id.getStartedButton);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
