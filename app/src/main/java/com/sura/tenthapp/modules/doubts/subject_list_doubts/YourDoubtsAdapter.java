package com.sura.tenthapp.modules.doubts.subject_list_doubts;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.doubts.doubt_lists.DoubtListPerSubject;

import org.json.JSONArray;

public class YourDoubtsAdapter extends RecyclerView.Adapter<YourDoubtsAdapter.ViewHolder> {

    public Context context;
    public JSONArray jsonArray;

    public YourDoubtsAdapter(Context askDoubt, JSONArray jsonArray) {
        this.context=askDoubt;
        this.jsonArray=jsonArray;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.doubts_subjects_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        holder.doubtQuestionText.setText(jsonArray.optJSONObject(position).optString("question"));
//        holder.doubtAnswerText.setText(jsonArray.optJSONObject(position).optString("answer"));

        holder.subjectDoubtName.setText(jsonArray.optJSONObject(position).optString("subject_name"));
        holder.totalDoubtText.setText("Total Doubts : "+jsonArray.optJSONObject(position).optString("doubts"));
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView doubtQuestionText,doubtAnswerText;

        public TextView subjectDoubtName,totalDoubtText;
        public RelativeLayout subjectsDoubtButton;


        public ViewHolder(View itemView) {
            super(itemView);
//            doubtAnswerText=itemView.findViewById(R.id.doubtAnswerText);
//            doubtQuestionText=itemView.findViewById(R.id.doubtQuestionText);

            subjectDoubtName=itemView.findViewById(R.id.subjectDoubtName);
            totalDoubtText=itemView.findViewById(R.id.totalDoubtText);
            subjectsDoubtButton=itemView.findViewById(R.id.subjectsDoubtButton);

            subjectsDoubtButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, DoubtListPerSubject.class);
                    intent.putExtra("from","general");
                    intent.putExtra("subject_id",jsonArray.optJSONObject(getAdapterPosition()).optString("subject_id"));
                    context.startActivity(intent);
                }
            });
        }
    }
} 