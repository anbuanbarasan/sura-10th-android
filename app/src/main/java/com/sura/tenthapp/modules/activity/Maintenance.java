package com.sura.tenthapp.modules.activity;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.HomeActivity;
import com.sura.tenthapp.utils.SharedHelperModel;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Maintenance extends AppCompatActivity {

    private Button offlineDownloadsButton;
    TextView desc1Textview,desc2Textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintenance);

        initViews();
        setListener();
        checkLogin();

        Intent i=getIntent();
        String desc1=i.getStringExtra("desc1");
        String desc2=i.getStringExtra("desc2");

        desc1Textview.setText(desc1);
        desc2Textview.setText(desc2);
    }

    private void checkLogin() {
        SharedHelperModel sharedHelperModel = new SharedHelperModel(Maintenance.this);
        String status = sharedHelperModel.getLoginStatus();
        Log.e("status", "run: " + sharedHelperModel.getLoginStatus());
        if (status.equals("0") || status.equals("")) {
            offlineDownloadsButton.setVisibility(View.GONE);
        } else{
            offlineDownloadsButton.setVisibility(View.VISIBLE);
        }
    }

    private void setListener() {
        offlineDownloadsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Maintenance.this,HomeActivity.class);
                intent.putExtra("from","maintenance");
                startActivity(intent);
                finish();
            }
        });
    }


    private void initViews() {
        desc1Textview=findViewById(R.id.desc1);
        desc2Textview =findViewById(R.id.desc2);
        offlineDownloadsButton =findViewById(R.id.offlineDownloadsButton);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
