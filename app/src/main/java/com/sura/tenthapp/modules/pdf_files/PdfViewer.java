package com.sura.tenthapp.modules.pdf_files;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.VolleyError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONObject;

import java.util.HashMap;

public class PdfViewer extends BaseActivity {

    private static final String TAG = "PdfViewer";
    private WebView webView;
    private Intent i;
    private ProgressDialog pd;
    private static String id="";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdf_viewer);
        webView=findViewById(R.id.webView);

        i=getIntent();

        //postVolleyResponse();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        final String pdf = i.getStringExtra("link");
        id=i.getStringExtra("id");

        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                setTitle("Loading...");
                setProgress(progress * 100); //Make the bar disappear after URL is loaded
                // Return the app name after finish loading
                if(progress == 100)
                    setTitle(R.string.app_name);
            }
        });

        webView.setWebViewClient(new HelloWebViewClient());
        pd = new ProgressDialog(PdfViewer.this);
        pd.setMessage("Loading Please wait..");
        pd.setCancelable(false);
        pd.show();
        webView.setWebViewClient(new HelloWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        String fullurl="https://docs.google.com/gview?embedded=true&url="+pdf;
        Log.d("onCreate: ",""+fullurl);
        webView.loadUrl(fullurl);

    }

    private void postVolleyResponse() {

        try{
            IResult mResultCallback = new IResult() {
                @Override
                public void notifySuccess(String requestType, JSONObject response) {
                    Log.e(TAG, "Volley requester " + requestType);
                    Log.e(TAG, "Volley JSON post" + response);

                }

                @Override
                public void notifySuccessString(String requestType, String response) {

                }

                @Override
                public void notifyError(String requestType, String error) {
                    Log.e(TAG, "Volley requester " + error);
                    Log.e(TAG, "Volley JSON post" + "That didn't work!");

                }
            };

            VolleyService mVolleyService = new VolleyService(mResultCallback, this);
            SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("authorization", sharedHelperModel.getAccessToken());
            Log.e(TAG, "volleyResponse : "+sharedHelperModel.getAccessToken() );
            HashMap<String, String> body = new HashMap<String, String>();
            body.put("id", id);
            Log.e(TAG, "volleyResponse: "+body );
            mVolleyService.postDataVolley("POSTCALL", UrlHelper.postPdfLog, body, headers);
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (!pd.isShowing()) {
                pd.show();
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
//            webView.loadUrl("javascript:(function() { " +
//                    "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()");
   webView.loadUrl("javascript:(function() { " +
                    "document.getElementsByClassName('ndfHFb-c4YZDc-Wrql6b')[0].style.visibility='hidden'; })()");

        }
    }
}
