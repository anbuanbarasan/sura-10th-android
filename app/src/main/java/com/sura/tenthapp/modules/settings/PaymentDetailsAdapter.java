package com.sura.tenthapp.modules.settings;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.DOWNLOAD_SERVICE;

public class PaymentDetailsAdapter extends RecyclerView.Adapter<PaymentDetailsAdapter.ViewHolder>  {

    private static final String TAG = PaymentDetailsAdapter.class.getSimpleName();
    public Context context;
    public JSONArray jsonArray;
    private DownloadManager mgr = null;
    private long lastDownload = -1L;


    public PaymentDetailsAdapter(Context context, JSONArray jsonArray) {

        this.context=context;
        this.jsonArray=jsonArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.paymentdetails_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.subjectName.setText(jsonArray.optJSONObject(position).optString("subject_name"));
        holder.orderId.setText(jsonArray.optJSONObject(position).optString("trans_id"));
        holder.status.setText(jsonArray.optJSONObject(position).optString("payment_status"));
        holder.transactionDate.setText(convertDateFormat(jsonArray.optJSONObject(position).optString("created_at")));
            holder.transactionTime.setText(convertTimeFormat(jsonArray.optJSONObject(position).optString("created_at").substring(jsonArray.optJSONObject(position).optString("created_at").length() - 8)));
        holder.transactionAmount.setText("\u20B9 "+jsonArray.optJSONObject(position).optString("amount"));

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView orderId,subjectName,status,transactionDate,transactionTime,transactionAmount;
        public Button downloadButton;

        public ViewHolder(View itemView) {
            super(itemView);

            orderId=itemView.findViewById(R.id.orderId);
            subjectName=itemView.findViewById(R.id.subjectNameText);
            downloadButton=itemView.findViewById(R.id.downloadInvoice);
            status=itemView.findViewById(R.id.status);
            transactionDate=itemView.findViewById(R.id.transactionDate);
            transactionTime=itemView.findViewById(R.id.transactionTime);
            transactionAmount=itemView.findViewById(R.id.transactionAmount);


            /*downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    int res = context.getApplicationContext().checkCallingOrSelfPermission(permission);
                    if(res == PackageManager.PERMISSION_GRANTED) {

                        getDoubtsVolleyResponse(jsonArray.optJSONObject(getAdapterPosition()).optString("id"));

                    }
                    else{
                        Toast.makeText(context, "Please Grant Storage Permission in Settings", Toast.LENGTH_SHORT).show();
                    }


                }
            });*/
        }
    }

    private void getDoubtsVolleyResponse(final String id) {

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
                else {


                 taskBegin(response.optString("pdf"));


                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("id",id);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postUserInvoice, body, headers);

    }

    private void taskBegin(String url) {

        Uri uri = Uri.parse(url);

        mgr = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);

        Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .mkdirs();

        lastDownload = mgr.enqueue(new DownloadManager.Request(uri)
                        .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                                DownloadManager.Request.NETWORK_MOBILE)
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setAllowedOverRoaming(true)
                        .setTitle("Sura's Invoice")
                        .setDescription("Downloading invoice")
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                                "Sura Invoice.pdf"));

        Toast.makeText(context, "Downloading Invoice", Toast.LENGTH_SHORT).show();


//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                boolean downloading = true;
//
//                while (downloading) {
//                    DownloadManager.Query q = new DownloadManager.Query();
//                    q.setFilterById(lastDownload);
//
//                    Cursor cursor = mgr.query(q);
//                    cursor.moveToFirst();
//                    int bytes_downloaded = cursor.getInt(cursor
//                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
//                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
//
//                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
//                        downloading = false;
//                    }
//
//                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            mProgressBar.setProgress((int) dl_progress);
//                            Log.e(TAG, "run: " + dl_progress);
//                        }
//                    });
//
//                    Log.d(TAG, statusMessage(cursor));
//                    cursor.close();
//                }
//            }
//        }).start();




    }

    private String statusMessage(Cursor c) {
        String msg = "???";

        switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Download failed!";
                break;

            case DownloadManager.STATUS_PAUSED:
                msg = "Download paused!";
                break;

            case DownloadManager.STATUS_PENDING:
                msg = "Download pending!";
                break;

            case DownloadManager.STATUS_RUNNING:
                msg = "Download in progress!";
                break;

            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Download complete!";
                break;

            default:
                msg = "Download is nowhere in sight";
                break;
        }

        return (msg);
    }

    public String convertDateFormat(String dateFormat) {

        Log.e(TAG, "convertDateFormat: "+dateFormat );
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateFormat);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String convertTimeFormat(String dateStr){

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {

        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);
        String _24HourTime = formattedDate;
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");


        Date _24HourDt = null;
        try {
            _24HourDt = _24HourSDF.parse(_24HourTime);
        } catch (ParseException e) {

        }


        return String.valueOf(_12HourSDF.format(_24HourDt));
    }



}