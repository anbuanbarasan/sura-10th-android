package com.sura.tenthapp.modules.settings;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiBanner;
import com.inmobi.sdk.InMobiSdk;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.PlacementId;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Feedback extends BaseActivity {

    private static final String TAG = "Feedback";
    private EditText feedbackEdit;
    private Button submitButton;
    private TextView keysText;
    InputFilter filter;
//    private AdView mAdView;
    private RelativeLayout backButton;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });



        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

//        mAdView.setAdListener(new AdListener(){
//            @Override
//            public void onAdLoaded() {
//                adLayout.setVisibility(View.VISIBLE);
//            }
//        });

        initViews();
        //setFilter();
//        feedbackEdit.setFilters(new InputFilter[]{filter});
        setListener();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");

    }

//    @Override
//    public void onBackPressed() {
//
//        Utils.hideSoftKeyboard(this);
//
//    }

    public void retryDialog(final String error) {
        final Dialog retryDialog = new Dialog(Feedback.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton = retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();

                postVolleyResponse();

            }
        });
        retryDialog.show();
    }


    private void setListener() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feedbackEdit.getText().toString().length() != 0) {
                    postVolleyResponse();
                } else {
                    feedbackEdit.setError(getResources().getString(R.string.fillThefield));
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Feedback.super.onBackPressed();
            }
        });
    }

    private void postVolleyResponse() {


        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                //Check error message on response
                if (response.optString("error").equals("true")) {

                    Toast.makeText(Feedback.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Feedback.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    Feedback.super.onBackPressed();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("feedback", feedbackEdit.getText().toString().trim());
        body.put("standard", getResources().getString(R.string.standard));

        SharedHelperModel sharedHelperModel = new SharedHelperModel(Feedback.this);
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postFeedback, body, header);


    }

    private void initViews() {
        feedbackEdit = findViewById(R.id.feedbackEdit);
        submitButton = findViewById(R.id.submitButton);
        keysText = findViewById(R.id.keysText);
        backButton = findViewById(R.id.backButton);
    }

    private void setFilter() {
        filter = new InputFilter() {
            boolean canEnterSpace = false;

            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if (feedbackEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }

                StringBuilder builder = new StringBuilder();

                for (int i = start; i < end; i++) {
                    char currentChar = source.charAt(i);

                    if (Character.isLetterOrDigit(currentChar) || currentChar == '_') {
                        builder.append(currentChar);
                        canEnterSpace = true;
                    }

                    if (Character.isWhitespace(currentChar) && canEnterSpace) {
                        builder.append(currentChar);
                    }


                }
                return builder.toString();
            }

        };

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
