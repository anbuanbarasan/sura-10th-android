package com.sura.tenthapp.modules.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.android.volley.VolleyError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.HomeActivity;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.language.LocaleUtils;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChooseMedium extends BaseActivity {

    private static final String TAG = "ChooseMedium";
    private CardView englishButton, tamilButton;
    private SharedHelperModel sharedHelperModel;
    private RelativeLayout backButton;
    private FirebaseAnalytics mFirebaseAnalytics;
    private View tamilBackground,englishBackground;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_medium);

        initViews();
        setListener();
        setHighlight();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setHighlight() {
        String lang = new SharedHelperModel(ChooseMedium.this).getLang();

        if (lang.equals("en")) {
            tamilBackground.setBackground(getResources().getDrawable(R.drawable.medium_border_unselect));
            englishBackground.setBackground(getResources().getDrawable(R.drawable.medium_border_selected));

        } else if (lang.equals("ta")) {
            tamilBackground.setBackground(getResources().getDrawable(R.drawable.medium_border_selected));
            englishBackground.setBackground(getResources().getDrawable(R.drawable.medium_border_unselect));
        }
    }

    private void setListener() {
        englishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                volleyResponse(1);
            }
        });
        tamilButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                volleyResponse(2);
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseMedium.super.onBackPressed();
            }
        });
    }

    public void retryDialog(final Context context, final int i, String error) {
        final Dialog retryDialog = new Dialog(ChooseMedium.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        Button retryButton = retryDialog.findViewById(R.id.retryButton);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();
                volleyResponse(i);
            }
        });
        retryDialog.show();
    }


    private void volleyResponse(final int i) {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                //Check error message on response
                if (response.optString("error").equals("true")) {

                    Toast.makeText(ChooseMedium.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    if (i == 1) {
                        sharedHelperModel.setLoginStatus("1");
                        sharedHelperModel.setLang("en");
                        Intent intent = new Intent(ChooseMedium.this, HomeActivity.class);
                        intent.putExtra("pdf", "0");
                        intent.putExtra("from", "medium");

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        LocaleUtils.setLocale(new Locale("en"));
                        LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                    } else if (i == 2) {
                        sharedHelperModel.setLoginStatus("1");
                        sharedHelperModel.setLang("ta");
                        Intent intent = new Intent(ChooseMedium.this, HomeActivity.class);
                        intent.putExtra("pdf", "0");
                        intent.putExtra("from", "medium");

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        LocaleUtils.setLocale(new Locale("ta"));
                        LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                    }


                }

            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if (!error.equalsIgnoreCase("Parse Error.. Please Try Again..")) {

                    retryDialog(ChooseMedium.this, i, error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("medium_id", String.valueOf(i));

        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.setMedium, body, header);

    }

    private void initViews() {
        englishButton = findViewById(R.id.englishButton);
        tamilButton = findViewById(R.id.tamilButton);
        backButton = findViewById(R.id.backButton);
        tamilBackground = findViewById(R.id.tamilbackground);
        englishBackground = findViewById(R.id.englishBackground);
        sharedHelperModel = new SharedHelperModel(ChooseMedium.this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
