package com.sura.tenthapp.modules.pdf_files;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;

import org.json.JSONArray;

import java.util.Random;

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {

    public Context context;
    public JSONArray jsonArray;
    public int i=0;

    public FilesAdapter(Context filesFragment, JSONArray response) {
        this.context=filesFragment;
        this.jsonArray=response;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.files_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int[] androidColors = context.getResources().getIntArray(R.array.calendarColors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        //holder.colorCard.setCardBackgroundColor(randomAndroidColor);

        if(i==4){
            i=0;
        }

        holder.colorCard.setBackgroundColor(androidColors[i]);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.subjectNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("title"),Html.FROM_HTML_MODE_LEGACY));
            holder.descriptionText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("subtitle"),Html.FROM_HTML_MODE_LEGACY));
            holder.chapterNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("summary"),Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.subjectNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("title")));
            holder.descriptionText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("subtitle")));
            holder.chapterNameText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("summary")));
        }

        i=i+1;

//        if (position == 0) {
//            holder.colorCard.setCardBackgroundColor(context.getResources().getColor(R.color.code1));
//        }else if (position == 1) {
//            holder.colorCard.setCardBackgroundColor(context.getResources().getColor(R.color.code2));
//        }else if (position == 2) {
//            holder.colorCard.setCardBackgroundColor(context.getResources().getColor(R.color.code3));
//        }else if (position == 3) {
//            holder.colorCard.setCardBackgroundColor(context.getResources().getColor(R.color.code4));
//        }else if (position == 4) {
//            holder.colorCard.setCardBackgroundColor(context.getResources().getColor(R.color.code5));
//        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout colorCard;
        public TextView subjectNameText,chapterNameText,descriptionText;
        public ViewHolder(View itemView) {
            super(itemView);
            colorCard=itemView.findViewById(R.id.colorCard);
            subjectNameText=itemView.findViewById(R.id.subjectNameText);
            chapterNameText=itemView.findViewById(R.id.chapterNameText);
            descriptionText=itemView.findViewById(R.id.descriptionText);

            colorCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, PdfViewer.class);
                    intent.putExtra("link",jsonArray.optJSONObject(getAdapterPosition()).optString("link"));
                    intent.putExtra("id",jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
                    context.startActivity(intent);
                }
            });
        }
    }
}