package com.sura.tenthapp.modules.ask_doubt;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sura.tenthapp.R;

import org.json.JSONArray;

public class OtherDoubtsAdapter extends RecyclerView.Adapter<OtherDoubtsAdapter.ViewHolder> {

    public Context context;
    public JSONArray jsonArray;

    public OtherDoubtsAdapter(Context askDoubt, JSONArray jsonArray) {
        this.context=askDoubt;
        this.jsonArray=jsonArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.otherdoubts_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.doubtQuestionText.setText(jsonArray.optJSONObject(position).optString("question"));
        holder.doubtAnswerText.setText(jsonArray.optJSONObject(position).optString("answer"));
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView doubtQuestionText,doubtAnswerText;

        public ViewHolder(View itemView) {
            super(itemView);
            doubtAnswerText=itemView.findViewById(R.id.doubtAnswerText);
            doubtQuestionText=itemView.findViewById(R.id.doubtQuestionText);

        }
    }
}