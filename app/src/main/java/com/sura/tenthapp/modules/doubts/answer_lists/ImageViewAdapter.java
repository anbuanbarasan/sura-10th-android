package com.sura.tenthapp.modules.doubts.answer_lists;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.pdf_files.PdfViewer;

import org.json.JSONArray;

public class ImageViewAdapter extends RecyclerView.Adapter<ImageViewAdapter.ViewHolder> {

    public Context context;
    public JSONArray jsonArray;
    public String type="";


    public ImageViewAdapter(Context context, JSONArray jsonArray, String type) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.type=type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_viewer_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(type.equalsIgnoreCase("image")){
            holder.imageViewerlayout.setVisibility(View.VISIBLE);
            holder.pdfLayout.setVisibility(View.GONE);
            holder.imageText.setText(getFileNameFromUrl(jsonArray.optJSONObject(position).optString("image")));
            Glide.with(context).load(jsonArray.optJSONObject(position).optString("image")).into(holder.answerImage);
        }else if(type.equalsIgnoreCase("pdf")){
            holder.imageViewerlayout.setVisibility(View.GONE);
            holder.pdfLayout.setVisibility(View.VISIBLE);
            holder.pdfText.setText(getFileNameFromUrl(jsonArray.optJSONObject(position).optString("pdf")));
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView answerImage;
        public LinearLayout imageViewerlayout,pdfLayout;
        public CardView pdfButton;
        private TextView imageText;
        private TextView pdfText;

        public ViewHolder(View itemView) {
            super(itemView);
            answerImage = itemView.findViewById(R.id.answerImage);
            imageViewerlayout = itemView.findViewById(R.id.imageViewerlayout);
            pdfLayout = itemView.findViewById(R.id.pdfLayout);
            pdfButton = itemView.findViewById(R.id.pdfButton);
            imageText = itemView.findViewById(R.id.imageText);
            pdfText = itemView.findViewById(R.id.pdfText);
            /*answerImage.setOnMatrixChangeListener(new MatrixChangeListener());*/

            answerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            pdfButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, PdfViewer.class);
                    intent.putExtra("link",jsonArray.optJSONObject(getAdapterPosition()).optString("pdf"));
                    intent.putExtra("id","");
                    context.startActivity(intent);
                }
            });
        }
    }

    public static String getFileNameFromUrl(String url) {


        return url.substring(url.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];
    }

/*
    private class MatrixChangeListener implements PhotoViewAttacher.OnMatrixChangedListener {

        @Override
        public void onMatrixChanged(RectF rect) {

        }
    }
*/


}