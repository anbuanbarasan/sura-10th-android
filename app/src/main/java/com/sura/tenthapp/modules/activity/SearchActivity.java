package com.sura.tenthapp.modules.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActivity extends BaseActivity {

    private static final String TAG = SearchActivity.class.getName();
    private EditText searchEdit;
    InputFilter filter;
    private String subjectId = "";
    private Intent i;
    private WebView webview;
    private RelativeLayout backButton;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        initViews();
        //setFilter();
        subjectId = i.getStringExtra("subjectId");
//        searchEdit.setFilters(new InputFilter[]{filter});
        setListener();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void setListener() {

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Utils.hideSoftKeyboard(SearchActivity.this);
                SearchActivity.super.onBackPressed();
            }
        });

        searchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if(searchEdit.getText().toString().trim().length()!=0){
                        performSearch();
                    }else{
                        searchEdit.setError(getResources().getString(R.string.fillThefield));
                    }

                    return true;
                }
                return false;
            }
        });
    }

    private void performSearch() {

         final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);

            }

            @Override
            public void notifySuccessString(String requestType, String response) {

                WebSettings webSettings = webview.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                webview.setVerticalScrollBarEnabled(true);
                webview.setScrollBarSize(3);
                webview.setHorizontalScrollBarEnabled(true);
                webview.getSettings().setBuiltInZoomControls(true);
                webview.getSettings().setDisplayZoomControls(false);
                webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        loaderDialog.dismiss();
                    }
                });
                webview.loadDataWithBaseURL(null, response, "text/html", "UTF-8", null);
            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_id", subjectId);
        body.put("search", searchEdit.getText().toString().trim());

        mVolleyService.postDataStringVolley("POSTCALL", UrlHelper.postSearch, body, headers);

    }

    private void setFilter() {
        filter = new InputFilter() {
            boolean canEnterSpace = false;

            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if (searchEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }

                StringBuilder builder = new StringBuilder();

                for (int i = start; i < end; i++) {
                    char currentChar = source.charAt(i);

                    if (Character.isLetterOrDigit(currentChar) || currentChar == '_') {
                        builder.append(currentChar);
                        canEnterSpace = true;
                    }

                    if (Character.isWhitespace(currentChar) && canEnterSpace) {
                        builder.append(currentChar);
                    }


                }
                return builder.toString();
            }

        };

    }

    private void initViews() {
        searchEdit = findViewById(R.id.searchEdit);
        webview = findViewById(R.id.webview);
        backButton = findViewById(R.id.backButton);
        i = getIntent();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
