package com.sura.tenthapp.modules.reading.chapter_list;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;

import org.json.JSONArray;
import org.json.JSONException;

public class ChapterLIstPaymentAdapter extends RecyclerView.Adapter<ChapterLIstPaymentAdapter.ViewHolder> {

    private static final String TAG = ChapterLIstPaymentAdapter.class.getSimpleName();
    private Context context;
    public static JSONArray jsonArray;
    static int cost=0;

    public ChapterLIstPaymentAdapter(Context chapter, JSONArray jsonArray) {
        this.context = chapter;
        ChapterLIstPaymentAdapter.jsonArray = jsonArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_list_payment_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.ChapterPrice.setText(jsonArray.optJSONObject(position).optString("cost"));

        if (jsonArray.optJSONObject(position).optString("chapter_status").equals("lock")) {
            holder.chapterListLayout.setVisibility(View.VISIBLE);
        } else {
            holder.chapterListLayout.setVisibility(View.GONE);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.chapterTitleText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("chapter_name"), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.chapterTitleText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("chapter_name")));
        }

        holder.chapterNumberText.setText(jsonArray.optJSONObject(position).optString("chapter_number"));
//        holder.chapterTitleText.setText(jsonArray.optJSONArray("list_chapter").optJSONObject(position).optString("chapter_name"));


        if (jsonArray.optJSONObject(position).optString("isSelected").equalsIgnoreCase("true")) {
            holder.checkChapter.setVisibility(View.VISIBLE);
        } else {
            holder.checkChapter.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView chapterNumberText;
        public TextView chapterTitleText,ChapterPrice;
        public RelativeLayout chapterListLayout;
        public ImageView checkChapter;

        public ViewHolder(View itemView) {
            super(itemView);

            chapterNumberText = itemView.findViewById(R.id.chapterNumberText);
            chapterTitleText = itemView.findViewById(R.id.chapterTitleText);
            chapterListLayout = itemView.findViewById(R.id.chapterListLayout);
            checkChapter = itemView.findViewById(R.id.checkChapter);
            ChapterPrice = itemView.findViewById(R.id.ChapterPrice);

            chapterListLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos=getAdapterPosition();

                    if (checkChapter.getVisibility() == View.GONE) {

                        try {
                            jsonArray.optJSONObject(getAdapterPosition()).put("isSelected", "true");
                            notifyDataSetChanged();

                            Log.e(TAG, "onpos:"+pos);
                            cost=cost+Integer.parseInt(jsonArray.optJSONObject(pos).optString("cost"));
                            ChapterListAdapter.totalAmount.setText(String.valueOf(cost));
                        } catch (JSONException e) {

                        }

                    } else if (checkChapter.getVisibility() == View.VISIBLE) {

                        try {
                            jsonArray.optJSONObject(getAdapterPosition()).put("isSelected", "false");
                            notifyDataSetChanged();

                            cost=cost-Integer.parseInt(jsonArray.optJSONObject(pos).optString("cost"));
                            ChapterListAdapter.totalAmount.setText(String.valueOf(cost));

                        } catch (JSONException e) {

                        }

                    }


                }
            });

        }
    }
}