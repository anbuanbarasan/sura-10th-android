package com.sura.tenthapp.modules.settings;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONObject;

import java.util.HashMap;

public class WebViewer extends AppCompatActivity {
    WebView webview;

    private static final String TAG = WebViewer.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        webview = findViewById(R.id.webView);
        Intent i = getIntent();
        final String url = i.getStringExtra("url");
        getResponse(url);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

    }

    private void getResponse(String url) {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);
        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {

            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                WebSettings webSettings = webview.getSettings();
                webSettings.setJavaScriptEnabled(true);
                if (Build.VERSION.SDK_INT >= 19) {
                    webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                } else {
                    webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                }
                webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
                webview.setVerticalScrollBarEnabled(true);
                webview.setScrollBarSize(2);
                webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        loaderDialog.dismiss();
                    }

                    @Override
                    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                       loaderDialog.dismiss();
                    }
                });
                webview.loadDataWithBaseURL(null, String.valueOf(response), "text/html", "UTF-8", null);
            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.getStringDataVolley("GETCALL", url, headers);
    }

}