package com.sura.tenthapp.modules.doubts.answer_lists;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.sura.tenthapp.R;

import org.json.JSONArray;
import org.json.JSONException;

public class AnswersImageViewer extends AppCompatActivity {

    private static final String TAG = AnswersImageViewer.class.getSimpleName();
    private RecyclerView imageViewRecyclerview;
    private RecyclerView.Adapter imageViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answers_image_viewer);

        initViews();
        Intent i=getIntent();

        if(i.getStringExtra("from").equalsIgnoreCase("pdf")){
            try {
                JSONArray jsonArray=new JSONArray(i.getStringExtra("pdfObject"));
                Log.e(TAG, "onCreate: "+i.getStringExtra("pdfObject") );
                setPdf(jsonArray);

            } catch (JSONException e) {

            }

        }else if(i.getStringExtra("from").equalsIgnoreCase("image")){
            try {
                JSONArray jsonArray=new JSONArray(i.getStringExtra("imageObject"));
                Log.e(TAG, "onCreate: "+i.getStringExtra("imageObject") );
                setImages(jsonArray);

            } catch (JSONException e) {

            }
        }




    }

    private void initViews() {
        imageViewRecyclerview=findViewById(R.id.imageViewRecyclerview);
    }

    private void setImages(JSONArray jsonArray) {
        imageViewAdapter = new ImageViewAdapter(this, jsonArray,"image");
        imageViewRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        imageViewRecyclerview.setHasFixedSize(true);
        imageViewRecyclerview.setFocusable(false);
        imageViewRecyclerview.setAdapter(imageViewAdapter);
    }
    private void setPdf(JSONArray jsonArray) {
        imageViewAdapter = new ImageViewAdapter(this, jsonArray,"pdf");
        imageViewRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        imageViewRecyclerview.setHasFixedSize(true);
        imageViewRecyclerview.setFocusable(false);
        imageViewRecyclerview.setAdapter(imageViewAdapter);
    }

}
