package com.sura.tenthapp.modules.login_register;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.sura.tenthapp.BuildConfig;
import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.HomeActivity;
import com.sura.tenthapp.modules.activity.ChooseMedium;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.language.LocaleUtils;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Enter_Otp extends BaseActivity {

    private static final String TAG = "Enter_Otp";
    Button confirmButton;
    EditText otpEdit;
    SharedHelperModel sharedHelperModel;
    private TextView resendOtpButton, countDownTimer;
    InputFilter filter;
    public static String otp = "";
    String abcd, mobileNumber;
    private static Dialog loaderDialog;
    private FirebaseAnalytics mFirebaseAnalytics;

//    private FirebaseAuth mAuth;
//    private PhoneAuthProvider.ForceResendingToken mResendToken;
//    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId = "";

    int resendCount = 1;

    private String version = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter__otp);
        initViews();
        Intent i = getIntent();
        mobileNumber = i.getStringExtra("mobileNumber");
        //setFilter();
        setListener();
        getVersionCode();

        setTimer();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
//        otpEdit.setText(sharedHelperModel.getOtp());

        if(BuildConfig.DEBUG){
            otpEdit.setEnabled(true);
            confirmButton.setVisibility(View.VISIBLE);
        }else{
            otpEdit.setEnabled(false);
            confirmButton.setVisibility(View.GONE);
        }


        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startPhoneNumberVerification(mobileNumber);
//            }
//        }, 500);


//        mAuth = FirebaseAuth.getInstance();
//        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
//            @Override
//            public void onVerificationCompleted(PhoneAuthCredential credential) {
//                Log.d(TAG, "onVerificationCompleted:" + credential);
//
//                signInWithPhoneAuthCredential(credential);
//            }
//
//            @Override
//            public void onVerificationFailed(FirebaseException e) {
//                Log.w(TAG, "onVerificationFailed", e);
//
//                if (e instanceof FirebaseAuthInvalidCredentialsException) {
//                    Toast.makeText(Enter_Otp.this, "Invalid Phone Number", Toast.LENGTH_SHORT).show();
//                } else if (e instanceof FirebaseTooManyRequestsException) {
//                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
//                            Snackbar.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
//
//                Log.d(TAG, "onCodeSent:" + verificationId);
//                mVerificationId = verificationId;
//                mResendToken = token;
//            }
//        };

        startSmsRetriever();
    }

    private void startSmsRetriever() {
        SmsRetrieverClient client = SmsRetriever.getClient(this /* context */);

// Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
                Toast.makeText(Enter_Otp.this, "Waiting for OTP", Toast.LENGTH_SHORT).show();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
                Toast.makeText(Enter_Otp.this, e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


//    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
//        mAuth.signInWithCredential(credential)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//
//                        if (task.isSuccessful()) {
//                            Log.d(TAG, "signInWithCredential:success");
//                            FirebaseUser user = task.getResult().getUser();
//                            volleyResponse();
//                        } else {
//                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
//                                otpEdit.setError("Invalid code.");
//                            }
//                        }
//                    }
//                });
//    }

//    private void verifyPhoneNumberWithCode(String verificationId, String code) {
//
//        if (verificationId.equals("")) {
//
//        } else {
//            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
//            signInWithPhoneAuthCredential(credential);
//        }
//    }


    private void setTimer() {
        new CountDownTimer(90000, 1000) {

            public void onTick(long millisUntilFinished) {
                countDownTimer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                resendOtpButton.setVisibility(View.VISIBLE);
                countDownTimer.setVisibility(View.GONE);
            }
        }.start();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                otpEdit.setText(message);
                confirmButton.performClick();
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    private void setListener() {

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(otpEdit.getText().toString().length()>0){

                    volleyResponse();
                }else{
                    otpEdit.setError("Please fill the field");
                }


                if (otpEdit.getText().toString().length() == 5) {

//                    if (sharedHelperModel.getOtp().equalsIgnoreCase(otpEdit.getText().toString().trim())) {

//                        if (sharedHelperModel.getNewUser().equals("0")) {
//
//
//                            Intent intents = new Intent(Enter_Otp.this, Register.class);
//                            startActivity(intents);
//
//
//                        } else {
//                            volleyResponse();
//                        }
//                    } else {
//                        otpEdit.setError("Invalid OTP");
//                    }

                } else if (otpEdit.getText().toString().length() == 6) {
//                    verifyPhoneNumberWithCode(mVerificationId, otpEdit.getText().toString().trim());
                }

//                if (otpEdit.getText().toString().length() == 5&&sharedHelperModel.getOtp().equalsIgnoreCase(otpEdit.getText().toString().trim())) {
//                    LoaderDialog.showLoader(Enter_Otp.this);
//                    if (sharedHelperModel.getNewUser().equals("0")) {
//                        LoaderDialog.dismissLoader();
//
//                                Intent intent = new Intent(Enter_Otp.this, Register.class);
//                                startActivity(intent);
//
//
//                    } else {
//                        volleyResponse();
//                    }
//                }else{
//                    otpEdit.setError("Invalid OTP");
//                }
            }
        });
        resendOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resendOtpButton.setVisibility(View.GONE);
                countDownTimer.setVisibility(View.VISIBLE);
                setTimer();
                otpVolleyResponse();

//                if (resendCount % 2 != 0) {
//                    otpVolleyResponse();
//                } else {
//                    startPhoneNumberVerification(mobileNumber);
//                }

                resendCount += 1;

            }
        });
    }

//    public void startPhoneNumberVerification(String phoneNumber) {
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                phoneNumber,        // Phone number to verify
//                60,                 // Timeout duration
//                TimeUnit.SECONDS,   // Unit of timeout
//                this,               // Activity (for callback binding)
//                mCallbacks);        // OnVerificationStateChangedCallbacks
//    }


    private void otpVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        try {

            IResult mResultCallback = new IResult() {

                @Override
                public void notifySuccess(String requestType, JSONObject response) {
                    Log.e(TAG, "Volley requester " + requestType);
                    Log.e(TAG, "Volley JSON post" + response);
                    try {
                        loaderDialog.dismiss();
                        if (response.optString("error").equals("true")) {
                            Toast.makeText(Enter_Otp.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                        } else {
                            sharedHelperModel.setOtp(response.optString("otp"));
                            sharedHelperModel.setNewUser(response.optString("error_message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void notifyError(String requestType, String error) {
                    try {
                        Log.e(TAG, "Volley requester " + error);
                        Log.e(TAG, "Volley JSON post" + "That didn't work!");
                        loaderDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void notifySuccessString(String requestType, String response) {

                }
            };

            VolleyService mVolleyService = new VolleyService(mResultCallback, this);

            HashMap<String, String> body = new HashMap<String, String>();
            body.put("mobile_no", mobileNumber);
            body.put("version", version);
            body.put("resend", "1");
            HashMap<String, String> header = new HashMap<String, String>();
            header.put("authorization", "");
            mVolleyService.postDataVolley("POSTCALL", UrlHelper.getOtp, body, header);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void getVersionCode() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            Log.e(TAG, "getVersionCode: " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void volleyResponse() {


        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "OtpVerification " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                //Check error message on response
                JSONArray jsonArray = new JSONArray();
                sharedHelperModel.setContinueStudyArray(jsonArray.toString());

                if (response.optString("error").equals("true")) {

                    if (response.optString("error_message").equals("your are not signup yet")) {

                        Intent intent = new Intent(Enter_Otp.this, Register.class);
                        startActivity(intent);
                    }

                    Toast.makeText(Enter_Otp.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    if (sharedHelperModel.getNewUser().equals("0")) {

                        Intent intents = new Intent(Enter_Otp.this, Register.class);
                        startActivity(intents);

                    } else {
                        sharedHelperModel.setAccessToken("Bearer " + response.optString("access_token"));
                        sharedHelperModel.setKeysCount(response.optString("keys_no"));
                        sharedHelperModel.setUserReferalCode(response.optString("referral_code"));
                        sharedHelperModel.setReferalCodeStatus(response.optString("referral_code_status"));
                        sharedHelperModel.setLoginStatus("1");
                        sharedHelperModel.setLoginStudentOrTeacher(response.optString("type"));

                        if (response.optString("medium_id").equals("1")) {
                            new SharedHelperModel(Enter_Otp.this).setLang("en");

                            Intent intent = new Intent(Enter_Otp.this, HomeActivity.class);
                            intent.putExtra("pdf", "0");
                            intent.putExtra("from", "otp");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            LocaleUtils.setLocale(new Locale("en"));
                            LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());


                        } else if (response.optString("medium_id").equals("2")) {

                            new SharedHelperModel(Enter_Otp.this).setLang("ta");
                            Intent intent = new Intent(Enter_Otp.this, HomeActivity.class);
                            intent.putExtra("pdf", "0");
                            intent.putExtra("from", "otp");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            LocaleUtils.setLocale(new Locale("ta"));
                            LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());

                        } else if (response.optString("medium_id").toLowerCase().equals("none")) {

                            Intent intent = new Intent(Enter_Otp.this, ChooseMedium.class);
                            startActivity(intent);

                        }
                    }
                }

            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Otp Verification " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("mobile_no", sharedHelperModel.getMobileNumber());
        if (otpEdit.getText().toString().trim().length() == 5) {
            body.put("otp", sharedHelperModel.getOtp());
        } else {
            body.put("otp", "success");
        }
//        body.put("otp", otpEdit.getText().toString().trim());
        body.put("otp", otpEdit.getText().toString());
        body.put("device_id", sharedHelperModel.getDeviceToken());
        body.put("standard", getResources().getString(R.string.standard));
        Log.e(TAG, "Params: " + body);
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization", "");
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.login, body, header);

    }

    private void initViews() {
        otpEdit = findViewById(R.id.otpEdit);
        confirmButton = findViewById(R.id.confirmButton);
        resendOtpButton = findViewById(R.id.resendOtpButton);
        countDownTimer = findViewById(R.id.countDownTimer);
        sharedHelperModel = new SharedHelperModel(Enter_Otp.this);
    }

    private void setFilter() {
        otpEdit.setFilters(new InputFilter[]{filter});
        filter = new InputFilter() {
            boolean canEnterSpace = false;

            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if (otpEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }

                StringBuilder builder = new StringBuilder();

                for (int i = start; i < end; i++) {
                    char currentChar = source.charAt(i);

                    if (Character.isLetterOrDigit(currentChar) || currentChar == '_') {
                        builder.append(currentChar);
                        canEnterSpace = true;
                    }

                    if (Character.isWhitespace(currentChar) && canEnterSpace) {
                        builder.append(currentChar);
                    }


                }
                return builder.toString();
            }

        };

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
