package com.sura.tenthapp.modules.login_register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.sura.tenthapp.R
import com.sura.tenthapp.modules.login_register.viewmodel.LoginOrRegisterVM
import kotlinx.android.synthetic.main.password.*

class PasswordSetup : AppCompatActivity() {

    lateinit var  loginOrRegisterVM: LoginOrRegisterVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.password)
        initViews()
        setListeners()
    }

    fun initViews(){
        loginOrRegisterVM = ViewModelProvider(this)[LoginOrRegisterVM::class.java]
    }

    fun setListeners(){
        button_confirm.setOnClickListener {
            if(loginOrRegisterVM.validatePassword()){

            }
        }
    }
}
