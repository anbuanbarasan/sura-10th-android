package com.sura.tenthapp.utils.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.activity.ExtraKeyActivity;
import com.sura.tenthapp.activity.HomeActivity;
import com.sura.tenthapp.modules.doubts.answer_lists.AnswersList;
import com.sura.tenthapp.utils.SharedHelperModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Notification";
    Bitmap bitmap = null;
    Handler handler = new Handler();
    JSONObject jsonObject;
    int nextIntent = 0;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        SharedHelperModel sharedHelperModel=new SharedHelperModel(getApplicationContext());
        sharedHelperModel.setDeviceToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        Log.d(TAG, "From:" + remoteMessage.getFrom());
        Log.d(TAG, "Message_data_payload:" + remoteMessage.getData());

        if (remoteMessage.getData().size() > 0) {

            jsonObject = new JSONObject(remoteMessage.getData());


            try {
                if (jsonObject.getString("image").equals("none")) {
                    bitmap=null;
                } else {
                    bitmap = getBitmapfromUrl(remoteMessage.getData().get("image"));
                }
            } catch (JSONException e) {

            }
            handleNow(jsonObject, bitmap, remoteMessage);
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message notification payload: " + remoteMessage.getNotification().getBody());
            jsonObject = new JSONObject(remoteMessage.getData());

            try {
                if (jsonObject.getString("image").toLowerCase().equals("none")) {
                    bitmap = null;
                } else {
                    bitmap = getBitmapfromUrl(jsonObject.getString("image"));
                }
            } catch (JSONException e) {
                Log.e(TAG, "onMessageReceived: " + e.getMessage());
            }
            handleNow(jsonObject, bitmap, remoteMessage);
        }
    }

    private void handleNow(final JSONObject jsonObject, Bitmap bitmap, RemoteMessage remoteMessage) {



        Intent intent = null;
        PendingIntent pendingIntent = null;

        if (jsonObject.optString("key").equals("general")) {

            nextIntent = 1;

        } else if (jsonObject.optString("key").equals("pdf")) {

            intent = new Intent(this, HomeActivity.class);
            intent.putExtra("pdf", "1");
            intent.putExtra("from","fcm");
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        } else if (jsonObject.optString("key").equals("doubts")) {

            intent = new Intent(this, AnswersList.class);
            intent.putExtra("question_id",jsonObject.optString("question_id"));
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        } else if (jsonObject.optString("key").equals("referral")) {

            new SharedHelperModel(this).setKeysCount(jsonObject.optString("keys_no"));

            intent = new Intent(this, ExtraKeyActivity.class);
            intent.putExtra("from", "fcm");
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


        } else {
            intent = new Intent(this, HomeActivity.class);
            intent.putExtra("pdf", "0");
            intent.putExtra("from","fcm");
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        }

        String channelId = "com.sura.tenthapp";
//        String channelName = "Notification";
//        int importance = NotificationManager.IMPORTANCE_HIGH;
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            NotificationChannel mChannel = new NotificationChannel(
//                    channelId, channelName, importance);
//            notificationManager.createNotificationChannel(mChannel);
//        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.logo);
            notificationBuilder.setColor(this.getResources().getColor(R.color.colorPrimaryDark));

        } else {
            notificationBuilder.setSmallIcon(R.drawable.logo);
        }

        notificationBuilder.setContentTitle(remoteMessage.getData().get("title"));
        notificationBuilder.setContentText(remoteMessage.getData().get("message"));
        if (bitmap != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
        }
        notificationBuilder.setAutoCancel(true);
        if (nextIntent == 1) {
            nextIntent = 0;
            notificationBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
        } else {
            notificationBuilder.setContentIntent(pendingIntent);
        }
        notificationBuilder.setVibrate(new long[]{Notification.DEFAULT_VIBRATE});
        notificationBuilder.setPriority(Notification.PRIORITY_HIGH);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel(
                    channelId,
                    "sura",
                    importance);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }
        notificationManager.notify(0, notificationBuilder.build());

    }



    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


} 