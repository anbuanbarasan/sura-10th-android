package com.sura.tenthapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.sura.tenthapp.R;

public class LoaderDialog {

    public static Dialog showLoader(Context context){

        Dialog loaderDialog = new Dialog(context);
        loaderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loaderDialog.setContentView(R.layout.loader_dialog);
        loaderDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        loaderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//      loaderDialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.color.transparentWhite));
        loaderDialog.getWindow().setGravity(Gravity.CENTER);
        loaderDialog.setCanceledOnTouchOutside(false);
        loaderDialog.setCancelable(false);
        loaderDialog.show();
        return loaderDialog;
    }
} 