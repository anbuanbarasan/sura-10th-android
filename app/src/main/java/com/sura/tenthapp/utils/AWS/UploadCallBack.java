package com.sura.tenthapp.utils.AWS;

public interface UploadCallBack {
    void result(boolean b, String url);
} 