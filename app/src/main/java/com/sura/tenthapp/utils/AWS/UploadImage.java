package com.sura.tenthapp.utils.AWS;

import android.content.Context;
import android.util.Log;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.sura.tenthapp.utils.Constants;

import java.io.File;

public class UploadImage {

    private static final String TAG = UploadImage.class.getSimpleName();

    public static void uploadFile(Context context, final File file, final UploadCallBack uploadCallBack) {

        AmazonS3 amazonS3Client = getClient(context);
        TransferUtility transferUtility = new TransferUtility(amazonS3Client, context);
        Log.d(TAG, "uploadFile: " + file.getAbsolutePath());
        TransferObserver transferObserver = transferUtility.upload(Constants.BUCKET_NAME, file.getName(), file);
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.e("image_state", state.toString());
                if (state.toString().equalsIgnoreCase("IN_PROGRESS")) {

                    Log.e(TAG, "onStateChanged: " );

                } else if (state.toString().equalsIgnoreCase("COMPLETED")) {

                    String url = Constants.BASES3_URL + file.getName();
                    uploadCallBack.result(true, url);

                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.e("image_state_percentage", String.valueOf(percentage));
            }

            @Override
            public void onError(int id, Exception ex) {

                uploadCallBack.result(true, ex.getMessage());

                Log.e("image_state_error", ex.getMessage());

            }
        });

    }


    public static AmazonS3Client getClient(Context context) {
        System.setProperty(SDKGlobalConfiguration.ENFORCE_S3_SIGV4_SYSTEM_PROPERTY, "true");
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(context, Constants.POOL_ID, Constants.REGION);
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);
        s3Client.setEndpoint(Constants.ENDPOINT);
        return s3Client;
    }
} 