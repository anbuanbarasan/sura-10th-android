package com.sura.tenthapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedHelpers {

    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;

    public static void putkey(Context context, String Key, String Value)
    {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.commit();
    }
    public static String getKey(Context contextGetKey, String Key) {
        String Value = "";
        try {
            sharedPreferences = contextGetKey.getSharedPreferences("Cache", Context.MODE_PRIVATE);
            Value = sharedPreferences.getString(Key, "");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return Value;
    }

    public static void clearSharedPreferences(Context context){
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().commit();
    }


}