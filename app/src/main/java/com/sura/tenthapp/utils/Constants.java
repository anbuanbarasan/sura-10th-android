package com.sura.tenthapp.utils;

import com.amazonaws.regions.Regions;
import com.sura.tenthapp.BuildConfig;

public class Constants {

//        public static String BASEURL= BuildConfig.PAYMENT_BASEURL; //LIVE
    public static String BASEURL="http://167.71.225.243/sura/public/api/";   //RESEARCH

    public static final String DATA_URL_CHECKSUMGEN = BASEURL+"payment_v7";

    public static final String VALUE_MID="SURapu25567872553374";

    public static final String VALUE_INDUSTYPE="Retail109";

    public static final String VALUE_CHANNELID="WAP";

    public static final String VALUE_WEBSITE="SURapuWAP";

    public static final String VALUE_CALLBACKURL="https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";


//    public static final String DATA_URL_CHECKSUMGEN = "http://188.166.228.50/sura_10/public/api/payment";
//
//    public static final String VALUE_MID="SURAPU77549903475044";
//
//    public static final String VALUE_INDUSTYPE="Retail";
//
//    public static final String VALUE_CHANNELID="WAP";
//
//    public static final String VALUE_WEBSITE="APP_STAGING";
//
//    public static final String VALUE_CALLBACKURL="https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";



    public static final String POOL_ID="ap-south-1:04a0ff7d-83a4-4c06-9847-6e5432f87300";

    public static final Regions REGION= Regions.AP_SOUTH_1;

    public static final String BUCKET_NAME="sura-school-user";

    public static final String ENDPOINT="https://s3.amazonaws.com";

    public static final String BASES3_URL="https://s3.ap-south-1.amazonaws.com/"+BUCKET_NAME+"/";



} 