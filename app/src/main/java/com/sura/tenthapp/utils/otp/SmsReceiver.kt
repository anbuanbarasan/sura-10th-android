package com.sura.tenthapp.utils.otp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.telephony.SmsMessage
import android.util.Log

import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status

class SmsReceiver : BroadcastReceiver() {
    internal var abcd: String? = null
    internal var pattern = "(\\d{6})"
    override fun onReceive(context: Context, intent: Intent) {

        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
            val extras = intent.extras
            val status = extras!!.get(SmsRetriever.EXTRA_STATUS) as Status

            when (status.statusCode) {
                CommonStatusCodes.SUCCESS -> {
                    // Get SMS message contents
                    val message = extras.get(SmsRetriever.EXTRA_SMS_MESSAGE) as String
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server.

                    Log.e("Retriver", "onReceive: $message")
                    var otps = message.replace("<#>OTP-", "").split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]

                    val smsIntent = Intent("otp")
                    smsIntent.putExtra("message", otps)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(smsIntent)
                }
                CommonStatusCodes.TIMEOUT -> {

                }
            }// Waiting for SMS timed out (5 minutes)
            // Handle the error ...
        }

        //        Bundle data = intent.getExtras();
        //        Object[] pdus = (Object[]) data.get("pdus");
        //        Log.e("PDUS", "onReceive: " + pdus.toString());
        //        String messageBody = "";
        //        for (int i = 0; i < pdus.length; i++) {
        //
        //            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
        //            String sender = smsMessage.getDisplayOriginatingAddress();
        //            messageBody = smsMessage.getMessageBody();
        //            Log.e("SMS", "onReceive: " + messageBody);
        //
        //        }
        //        if (messageBody.contains("OTP")) {
        //            abcd = messageBody.replaceAll("[^0-9]", "");
        //            Log.e("sms", "onReceive: " + abcd);
        //            Intent smsIntent = new Intent("otp");
        //            smsIntent.putExtra("message", abcd);
        //            LocalBroadcastManager.getInstance(context).sendBroadcast(smsIntent);
        //        }
    }
}
