package com.sura.tenthapp.utils.language;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {



    public BaseActivity() {
        LocaleUtils.updateConfig(this);
    }
} 