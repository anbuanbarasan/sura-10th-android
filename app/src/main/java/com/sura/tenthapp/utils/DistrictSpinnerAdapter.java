package com.sura.tenthapp.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DistrictSpinnerAdapter extends ArrayAdapter<DistrictModel> {
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private ArrayList<DistrictModel> districtModelArrayList;
    public DistrictSpinnerAdapter(@NonNull Context context, int resource, ArrayList<DistrictModel> objects) {
        super(context, resource, objects);
        this.context=context;
        this.districtModelArrayList=objects;
    }


    @Override
    public int getCount(){
        return districtModelArrayList.size();
    }

    @Override
    public DistrictModel getItem(int position){
        return districtModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view=super.getView(position,convertView,parent);
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setTextSize(13);
        label.setText(districtModelArrayList.get(position).getDistrictName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(districtModelArrayList.get(position).getDistrictName());

        return label;
    }
}
